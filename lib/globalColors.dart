// ignore_for_file: constant_identifier_names

import 'package:flutter/material.dart';

class AppColors {
  static const Color primaryColor = Color.fromRGBO(63, 81, 181, 1);
  static const Color primaryColorDisable = Color.fromRGBO(63, 81, 181, 0.25);
  static const Color textColor = Color.fromRGBO(15, 15, 15, 1);
  static const Color white = Color.fromRGBO(255, 255, 255, 1);
  static const Color red = Color.fromRGBO(255, 0, 0, 1);
  static const Color disabledTextColor = Color.fromRGBO(102, 102, 102, 1);
  static const Color FlutterDefautBackground = Color.fromRGBO(250, 250, 250, 1);
  
}