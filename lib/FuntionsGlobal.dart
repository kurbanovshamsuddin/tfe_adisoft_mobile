import 'package:flutter/material.dart';
import 'package:adisoft_mobile/pages/PagesNavigationFooter/QRScan.dart';
import 'package:adisoft_mobile/pages/PagesNavigationFooter/Statistiques.dart';
import 'package:adisoft_mobile/pages/PagesNavigationFooter/fournisseurs.dart';
import 'package:adisoft_mobile/pages/PagesNavigationFooter/homepage.dart';
import 'pages/PagesNavigationFooter/clients.dart';





// import 'package:dropdown_textfield/dropdown_textfield.dart';


void footerNavigationSwitch(int number, BuildContext context) {
  switch(number) {
  case 0:
    Navigator.of(context).push(
      MaterialPageRoute(builder: (context) => pageQRscan())
    );
    break; 
  case 1:
        Navigator.of(context).push(
      MaterialPageRoute(builder: (context) => pageStatestiques())
    );
    break;
  case 2:
        Navigator.of(context).push(
      MaterialPageRoute(builder: (context) => HomePage())
    );
    break;
 case 3:
        Navigator.of(context).push(
      MaterialPageRoute(builder: (context) => pageFournisseurs())
    );
    break;
 case 4:
        Navigator.of(context).push(
      MaterialPageRoute(builder: (context) => pageClient())
    );
    break;
}
}


int calculerCleTVA(String numeroTVA) {
  // Supprimer les deux derniers chiffres du numéro de TVA
  String numeroSansDerniersChiffres = numeroTVA.substring(0, numeroTVA.length - 2);
  // Convertir en entier
  int numeroSansDerniersChiffresInt = int.parse(numeroSansDerniersChiffres.replaceAll('.', ''));
  // Effectuer la division
  double resultatDivision = numeroSansDerniersChiffresInt / 97;
  // Retrouver l'entier en ignorant les décimales
  int resultatDivisionInt = resultatDivision.toInt();
  // Multiplier le résultat par 97
  int resultatFinal = resultatDivisionInt * 97;
  // Calculer la différence
  int difference = numeroSansDerniersChiffresInt - resultatFinal;
  // Calculer la clé TVA
  int cleTVA = 97 - difference;
  return cleTVA;
}

// StatefulWidget buildDropdownField(List<String> options, String searchHint, String fieldHint) {
//   final SingleValueDropDownController controller =
//       SingleValueDropDownController();
//   return DropDownTextField(
//     dropDownItemCount: 3, 
//     controller: controller,
//     clearOption: true,
//     enableSearch: true,
//     searchDecoration:
//       InputDecoration(hintText: searchHint),
//     validator: (value) {
//       if (value == null) {
//         return "Required field";
//       } else {
//         return null;
//       }
//     },
//     textFieldDecoration: InputDecoration(hintText: fieldHint),
//     // dropDownItemCount: options.length,
//     dropDownList: options
//         .map((option) => DropDownValueModel(
//               name: option.length > 30 ? option.substring(0, 30) + "..." : option,
//               value: option,
//             ))
//         .toList(),
//     onChanged: (val) {},
//   );
// }
