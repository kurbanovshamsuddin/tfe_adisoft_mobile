// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables, library_private_types_in_public_api

import 'dart:convert';

import 'package:adisoft_mobile/pages/Login/db_connection.dart';
import 'package:adisoft_mobile/pages/PagesContact/listeDesContact.dart';
import 'package:flutter/material.dart';
import 'package:adisoft_mobile/FuntionsGlobal.dart';
import 'package:adisoft_mobile/globalColors.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;

class pageModificationContact extends StatefulWidget {
  final Map<String, dynamic> selectedClient;
  const pageModificationContact({Key? key, required this.selectedClient}) : super(key: key);

  @override
  _pageModificationContactState createState() => _pageModificationContactState();
}

class _pageModificationContactState extends State<pageModificationContact> {
  int _selectedIndex = 4;
  double tempDouble = 0;
  int IntThirdPartyId = 0;
  int ContId = 0;
  late TextEditingController firstName;
  late TextEditingController lastName;
  late TextEditingController mail;
  late TextEditingController phone;
  late TextEditingController street;
  late TextEditingController streetNum;
  late TextEditingController postalBox;
  late TextEditingController postalCode;
  late TextEditingController city;
  late TextEditingController country;
  late TextEditingController thirdPartyId;
  

  
  @override
  void initState() {
    super.initState();
    firstName = TextEditingController(text: widget.selectedClient['firstName']);
    lastName = TextEditingController(text: widget.selectedClient['lastName']);
    mail = TextEditingController(text: widget.selectedClient['mail']);
    phone = TextEditingController(text: widget.selectedClient['phone1']);
    street = TextEditingController(text: widget.selectedClient['street']);
    streetNum = TextEditingController(text: widget.selectedClient['streetNum']);
    postalBox = TextEditingController(text: widget.selectedClient['postalBox']);
    postalCode = TextEditingController(text: widget.selectedClient['postalCode']);
    city = TextEditingController(text: widget.selectedClient['city']);
    country = TextEditingController(text: widget.selectedClient['country']);
    // thirdPartyId = TextEditingController(text: widget.selectedClient['thirdPartyId'].toString());
    tempDouble = widget.selectedClient['thirdPartyId'];
    IntThirdPartyId = tempDouble.toInt();
    tempDouble = widget.selectedClient['id'];
    ContId = tempDouble.toInt();
    }

Future getCoutryISO(String num) async {
  final uri = Uri.parse('https://internaldemo.adisoft.app/ui/commonservices/phone/get-country-iso?phoneNumber=$num');
  try {
    final response = await http.get(
      uri,
      headers: {
        'Content-Type': 'application/json',
        'Referer': 'https://internaldemo.adisoft.app',
        'Cookie': LoginResponse.cookie,
      },
    );
    if (response.statusCode == 200) {
      print("pays = $response");
       Map<String, dynamic> responseMap = json.decode(response.body);
      format(responseMap['response'], num);
    } else {
      print('Error: ${response.statusCode}');
    }
    return response;
  } catch (e) {
    print('Exception error: $e');
    return null;
  }
}
Future format(String typeNum, String num) async {
  final uri = Uri.parse('https://internaldemo.adisoft.app/ui/commonservices/phone/format');
  try {
    final response = await http.post(
      uri,
      headers: {
        'Content-Type': 'application/json',
        'Referer': 'https://internaldemo.adisoft.app',
        'Cookie': LoginResponse.cookie,
      },
      body: jsonEncode({
      'countryIso': typeNum,
      'phoneNumber': num,
    }),
    );
    if (response.statusCode == 200) {
      print("pays = $response");
      Map<String, dynamic> responseMap = json.decode(response.body);
      setState(() {
        phone.text = responseMap['response'];
      });
    } else {
      print('Error: ${response.statusCode}');
    }
    return response;
  } catch (e) {
    print('Exception error: $e');
    return null;
  }
}

  void _navigateBottomBar(int index) {
    setState(() {
      _selectedIndex = index;
      footerNavigationSwitch(_selectedIndex, context);
    });
  }

@override
Widget build(BuildContext context) {
  return Scaffold(
    appBar: AppBar(
      leading: BackButton(
        color: AppColors.white,
      ),
      title: const Text("", style: TextStyle(color: Color.fromARGB(255, 17, 16, 16))),
      centerTitle: true,
      backgroundColor: AppColors.primaryColor,
    ),
    body: Column(
      children: [
        // Fixed content at the top
        Padding(
          padding: const EdgeInsets.all(10),
          child: Center(
            child: RichText(
              text: TextSpan(
                children: [
                  TextSpan(
                    text: 'modification d\'un ',
                    style: const TextStyle(
                      fontSize: 30,
                      color: AppColors.textColor,
                    ),
                  ),
                  TextSpan(
                    text: 'contact',
                    style: const TextStyle(
                      fontSize: 30,
                      color: AppColors.primaryColor,
                    ),
                  ),
                ],
              ),
              overflow: TextOverflow.ellipsis,
            ),
          ),
        ),
        // Scrollable content
         Expanded(
          child: SingleChildScrollView(
            child: Padding(
              padding: EdgeInsets.symmetric(vertical: 20,horizontal: 20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  TextField(
                    controller: firstName,
                    style: TextStyle(color: AppColors.textColor),
                    decoration: InputDecoration(
                      hintText: 'Prénom',
                      contentPadding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
                      border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(4)),
                    ),
                    filled: true, 
                    fillColor:AppColors.FlutterDefautBackground, 
                    hintStyle: TextStyle(color: AppColors.textColor), 
                    disabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: AppColors.textColor), 
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: AppColors.primaryColor, width: 2.0), 
                    ),
                    ),
                    
                  ),
                  SizedBox(height: 10),
                  TextField(
                     controller: lastName,
                      style: TextStyle(color: AppColors.textColor),
                    decoration: InputDecoration(
                      hintText: 'Nom',
                      contentPadding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
                      border: OutlineInputBorder( 
                      borderRadius: BorderRadius.all(Radius.circular(4)),
                    ),
                    filled: true, // Add background color
                    fillColor:AppColors.FlutterDefautBackground, 
                    hintStyle: TextStyle(color: AppColors.textColor), 
                    disabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: AppColors.textColor), 
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: AppColors.primaryColor, width: 2.0), 
                    ),
                    ),
                  ),
                  SizedBox(height: 10),
                  TextField(
                      controller: mail,
                      style: TextStyle(color: AppColors.textColor),
                      decoration: InputDecoration(
                      hintText: 'Mail',
                      contentPadding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
                      border: OutlineInputBorder( 
                      borderRadius: BorderRadius.all(Radius.circular(4)),
                    ),
                    filled: true, 
                    fillColor:AppColors.FlutterDefautBackground,
                    hintStyle: TextStyle(color: AppColors.textColor), 
                    disabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: AppColors.textColor), 
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: AppColors.primaryColor, width: 2.0), 
                    ),
                    ),
                  ),
                  SizedBox(height: 10),
                  Focus(
                    onFocusChange: (hasFocus) {
                      if (!hasFocus) {
                        // Call getCountryISO when focus is lost
                        getCoutryISO(phone.text.replaceAll(' ', ''));
                      }
                    },
                    child: TextField(
                      controller: phone,
                      keyboardType: TextInputType.phone,
                      inputFormatters: [
                       FilteringTextInputFormatter.allow(RegExp(r'^[()+\d -]{1,15}')),
                      ],
                      style: TextStyle(color: AppColors.disabledTextColor),
                      decoration: InputDecoration(
                        hintText: 'Numéro Téléphone',
                        contentPadding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(4)),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: AppColors.primaryColor, width: 2.0),
                        ),
                      ),
                      
                    ),
                  ),
                  // TextField(
                  //   controller: phone,
                  //   style: TextStyle(color: AppColors.textColor),
                  //   decoration: InputDecoration(
                  //     hintText: 'Numéro Téléphone',
                  //     contentPadding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
                  //     border: OutlineInputBorder( 
                  //     borderRadius: BorderRadius.all(Radius.circular(4)),
                  //   ),
                  //   filled: true, 
                  //   fillColor:AppColors.FlutterDefautBackground, 
                  //   hintStyle: TextStyle(color: AppColors.textColor), 
                  //   disabledBorder: OutlineInputBorder(
                  //     borderSide: BorderSide(color: AppColors.textColor), 
                  //   ),
                  //   focusedBorder: OutlineInputBorder(
                  //     borderSide: BorderSide(color: AppColors.primaryColor, width: 2.0), 
                  //   ),
                  //   ),
                  // ),
                  SizedBox(height: 10),
                  Center( 
                  child: Text(
                    "Adresse",
                    style: TextStyle(
                      fontSize: 30,
                      color: AppColors.primaryColor,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                ),
                  SizedBox(height: 10),
                  TextField(
                    controller: street,
                    style: TextStyle(color: AppColors.textColor),
                    decoration: InputDecoration(
                      hintText: 'Rue',
                      contentPadding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
                      border: OutlineInputBorder( 
                      borderRadius: BorderRadius.all(Radius.circular(4)),
                    ),

                    filled: true, 
                    fillColor:AppColors.FlutterDefautBackground, 
                    hintStyle: TextStyle(color: AppColors.textColor),
                    disabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: AppColors.textColor),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: AppColors.primaryColor, width: 2.0),
                    ),
                    ),
                  ),
                  SizedBox(height: 10),
                  Row(
                    children: [
                      Expanded(
                        child: TextField(
                          controller:  streetNum,
                          style: TextStyle(color: AppColors.textColor),
                          decoration: InputDecoration(
                            hintText: 'Numéro',
                            contentPadding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
                      border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(4)),
                    ),
                    filled: true,
                    fillColor:AppColors.FlutterDefautBackground,
                    hintStyle: TextStyle(color: AppColors.textColor),
                    disabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: AppColors.textColor),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: AppColors.primaryColor, width: 2.0),
                    ),
                          ),
                        ),
                      ),
                      SizedBox(width: 10),
                      Expanded(
                        child: TextField(
                          controller: postalBox,
                          style: TextStyle(color: AppColors.textColor),
                          decoration: InputDecoration(
                            hintText: 'Boîte postal',
                            contentPadding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
                      border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(4)),
                    ),
                    filled: true,
                    fillColor:AppColors.FlutterDefautBackground,
                    hintStyle: TextStyle(color: AppColors.textColor),
                    disabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: AppColors.textColor),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: AppColors.primaryColor, width: 2.0),
                    ),
                          ),
                        ),
                      ),
                    ],
                  ),

                  SizedBox(height: 10),
                  TextField(
                    controller: postalCode,
                    style: TextStyle(color: AppColors.textColor),
                    decoration: InputDecoration(
                      hintText: 'Code postal',
                      contentPadding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
                      border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(4)),
                    ),
                    filled: true,
                    fillColor:AppColors.FlutterDefautBackground,
                    hintStyle: TextStyle(color: AppColors.textColor),
                    disabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: AppColors.textColor),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: AppColors.primaryColor, width: 2.0),
                    ),
                    ),
                  ),
                  SizedBox(height: 10),
                  TextField(
                    controller: city,
                    style: TextStyle(color: AppColors.textColor),
                    decoration: InputDecoration(
                      hintText: 'Ville',
                      contentPadding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
                      border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(4)),
                    ),
                    filled: true,
                    fillColor:AppColors.FlutterDefautBackground,
                    hintStyle: TextStyle(color: AppColors.textColor),
                    disabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: AppColors.textColor),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: AppColors.primaryColor, width: 2.0),
                    ),
                    ),
                  ),
                  SizedBox(height: 10),
                  TextField(
                    controller: country,
                    style: TextStyle(color: AppColors.textColor),
                    decoration: InputDecoration(
                      hintText: 'Pays',
                      contentPadding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
                      border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(4)),
                    ),
                    filled: true,
                    fillColor:AppColors.FlutterDefautBackground,
                    hintStyle: TextStyle(color: AppColors.textColor),
                    disabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: AppColors.textColor),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: AppColors.primaryColor, width: 2.0),
                    ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
        Row(
  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
  children: [
    SizedBox(
      width: MediaQuery.of(context).size.width * 0.9,
      child: ElevatedButton(
        onPressed: () {

          Future addContactToUser() async {
            final uri = Uri.parse('https://internaldemo.adisoft.app/ui/signaletique/companycontact/save-contact');
            try {
              final response = await http.post(
                uri,
                headers: {
                  'Content-Type': 'application/json',
                  'Referer': 'https://internaldemo.adisoft.app',
                  'Cookie': LoginResponse.cookie,
                },
                body: jsonEncode({
                  "companyContact": {
                  'firstName': firstName.text,
                  'lastName': lastName.text,
                  'thirdPartyId': IntThirdPartyId,
                  'id': ContId,
                  'mail': mail.text,
                  'phone1': phone.text,
                  'street': street.text,
                  'streetNum': streetNum.text,
                  'postalBox': postalBox.text,
                  'postalCode': postalCode.text,
                  'city': city.text,
                  'country': country.text,
                  'language': 'fr', 
                  },
                  "businessFolderJournals": [],
                }),
              );
              if (response.statusCode == 200) {
                print("insert succesull");
                Navigator.of(context).pushReplacement(
                        MaterialPageRoute(builder: (context) => pagelisteDesContacts(selectedContact: widget.selectedClient)),
                );
              } else {
                print('Error: ${response.statusCode}');
              }
              
              return null;
              
            } catch (e) {
              print('Exception error: $e');
              return null;
            }
          }
          addContactToUser();
        },
        style: ElevatedButton.styleFrom(
          backgroundColor: AppColors.primaryColor, 
          foregroundColor: AppColors.white, 
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(4), 
          ),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text('Confirmer   '),
            Icon(Icons.check),
          ],
        ),
      ),
    ),
  ],
),

      ],
    ),
    
    bottomNavigationBar: BottomNavigationBar(
      currentIndex: _selectedIndex,
      onTap: _navigateBottomBar,
      type: BottomNavigationBarType.fixed,
      items: const [
        BottomNavigationBarItem(icon: Icon(Icons.home), label: 'QR code'),
        BottomNavigationBarItem(icon: Icon(Icons.message), label: 'Statistiques'),
        BottomNavigationBarItem(icon: Icon(Icons.add), label: 'Accueil'),
        BottomNavigationBarItem(icon: Icon(Icons.notifications), label: 'Fournisseurs'),
        BottomNavigationBarItem(icon: Icon(Icons.person), label: 'Clients'),
      ],
      selectedItemColor: AppColors.primaryColor,
      unselectedItemColor: AppColors.primaryColorDisable,
    ),
  );
}

}
