// ignore_for_file: prefer_const_literals_to_create_immutables, prefer_const_constructors, camel_case_types, prefer_final_fields, library_private_types_in_public_api, use_super_parameters, avoid_print, file_names

import 'dart:convert';
import 'package:adisoft_mobile/pages/Login/db_connection.dart';
import 'package:adisoft_mobile/pages/PagesContact/CRUD/afficheContact.dart';
import 'package:adisoft_mobile/pages/PagesContact/CRUD/nouveauContact.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import 'package:adisoft_mobile/FuntionsGlobal.dart';
import 'package:adisoft_mobile/globalColors.dart';

class pagelisteDesContacts extends StatefulWidget {
  // const pagelisteDesContacts({Key? key}) : super(key: key);
  final Map<String, dynamic> selectedContact;
  // const pagelisteDesContacts({Key? key, required this.selectedContact, required selectedClient}) : super(key: key);
 const pagelisteDesContacts({Key? key, required this.selectedContact}) : super(key: key);
  @override
  _pagelisteDesContactsState createState() => _pagelisteDesContactsState();
}

class _pagelisteDesContactsState extends State<pagelisteDesContacts> {
  int _selectedIndex = 4;
  List<Map<String, dynamic>> _tableData = [];
  TextEditingController _searchController = TextEditingController();
  @override
  void initState() {
    super.initState();
    fetchData((widget.selectedContact['thirdPartyId']).toInt());
  }

  Future<void> fetchData(int id) async {
    final uri = Uri.parse(
        'https://internaldemo.adisoft.app/ui/signaletique/companycontact/get-company-contacts?thirdPartyId=$id');
    final response = await http.get(
      uri,
      headers: {
        'Content-Type': 'application/json',
        'Referer': 'https://internaldemo.adisoft.app',
        'Cookie': LoginResponse.cookie,
      },
    );

    if (response.statusCode == 200) {
      // Successful response
      List<Map<String, dynamic>> dataList = [];
      final Map<String, dynamic> responseData = json.decode(utf8.decode(response.bodyBytes));
      // print(responseData.keys);
      final List<dynamic> responseList = responseData['response'];
      // print(responseList);
      // final List<Map<String, dynamic>> dataList =
      //     responseList.map((item) => item as Map<String, dynamic>).toList();

      for (var item in responseList) {
        // Check if the item is a Map<String, dynamic>
        if (item is Map<String, dynamic>) {
          // Check if deactivationDateTime is not empty
          if (item['deactivationDateTime'] == null) {
            // If it's not empty, add it to the dataList
            dataList.add(item);
          }
        }
      }
          // print(dataList);
      setState(() {
        _tableData = dataList;
      });
    } else {
      // Handle error
      print('Error: ${response.statusCode}');
    }
  }

  List<Map<String, dynamic>> _filterData(String query) {
    return _tableData.where((item) {
      // Combine all fields into a single string for searching
      String combinedInfo =
          "${item['lastName']} ${item['firstName']} ${item['mail']} ${item['phone1']}"
              .toLowerCase();
      return combinedInfo.contains(query.toLowerCase());
    }).toList();
  }

  void _navigateBottomBar(int index) {
    setState(() {
      _selectedIndex = index;
      footerNavigationSwitch(_selectedIndex, context);
    });
  }

  void _handleRowClick(int index) {
    final selectedClient = _filterData(_searchController.text)[index]; // Retrieve the entire item
    print(selectedClient);
    Navigator.of(context).push(
      // MaterialPageRoute(builder: (context) => pageAfficheContact(selectedContact: widget.selectedContact)),
      MaterialPageRoute(builder: (context) => pageAfficheContact(selectedClient: selectedClient, id: widget.selectedContact['id'])),  
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: BackButton(
          color: AppColors.white,
        ),
        title: const Text("Liste des contacts",
            style: TextStyle(color: AppColors.white)),
        centerTitle: true,
        backgroundColor: AppColors.primaryColor,
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          SizedBox(height: 5),
        Padding(
          padding: const EdgeInsets.all(10),
          child: Center(
            child: RichText(
              text: TextSpan(
                children: [
                  TextSpan(
                    text: widget.selectedContact['primaryName'],
                    style: const TextStyle(
                      fontSize: 30,
                      color: AppColors.primaryColor, 
                    ),
                  ),
                ],
              ),
              overflow: TextOverflow.ellipsis,
            ),
          ),
        ),
          SizedBox(height: 5),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10),
            child: Row(
              children: [
                Expanded(
                  child: TextField(
                    controller: _searchController,
                    onChanged: (value) {
                      setState(() {});
                    },
                    decoration: InputDecoration(
                      contentPadding: EdgeInsets.symmetric(vertical: 10),
                      hintText: 'Rechercher',
                      prefixIcon: Icon(Icons.search),
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(12),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(12),
                        borderSide: BorderSide(
                            width: 2,
                            color: AppColors.primaryColor),
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(12),
                        borderSide: BorderSide(width: 2, color: Colors.grey),
                      ),
                    ),
                  ),
                ),
                SizedBox(width: 5),
                ElevatedButton.icon(

                  onPressed: () {
                    Navigator.of(context).push(
                      MaterialPageRoute(builder: (context) => pageNouveauContact(partyId: widget.selectedContact['thirdPartyId'].toInt())),
                    );
                  },
                  icon: Icon(
                    Icons.add,
                    size: 32,
                    color: Colors.white,
                  ),
                  label: Text(''),
                  style: ElevatedButton.styleFrom(
                    padding: EdgeInsets.symmetric(horizontal: 8, vertical: 8),
                    alignment: Alignment(1, 0),
                    backgroundColor: AppColors
                        .primaryColor, // Change this to your desired color
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(12),
                    ),
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height: 20),
          Expanded(
            child: ListView.builder(
              padding: EdgeInsets.all(15),
              itemCount: _filterData(_searchController.text).length,
              itemBuilder: (BuildContext context, int index) {
                final filteredData = _filterData(_searchController.text);
                return GestureDetector(
                  onTap: () {
                    _handleRowClick(index);
                  },
                  child: Container(
                    decoration: BoxDecoration(
                      border: Border(
                        top: BorderSide(color: Colors.grey.withOpacity(0.5)),
                      ),
                    ),
                    child: Stack(
                      children: [
                        Row(
                          children: [
                            Expanded(
                              child: Card(
                                elevation: 0,
                                child: ListTile(
                                  contentPadding: EdgeInsets.zero,
                                  subtitle: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Row(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Expanded(
                                            child: Text(
                                              '${filteredData[index]['firstName']}',
                                              overflow: TextOverflow.ellipsis,
                                              maxLines: 1,
                                            ),
                                          ),
                                          Expanded(
                                            child: Text(
                                              '${filteredData[index]['lastName']}',
                                              overflow: TextOverflow.ellipsis,
                                              maxLines: 1,
                                            ),
                                          ),
                                        ],
                                      ),
                                  Row(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Expanded(
                                            child: Text(
                                              '${filteredData[index]['mail']} - ',
                                              overflow: TextOverflow.ellipsis,
                                              maxLines: 1,
                                            ),
                                          ),
                                          Expanded(
                                            child: Text(
                                              '${filteredData[index]['phone1']}',
                                              overflow: TextOverflow.ellipsis,
                                              maxLines: 1,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                );
              },
            ),
          ),
        ],
      ),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _selectedIndex,
        onTap: _navigateBottomBar,
        type: BottomNavigationBarType.fixed,
        items: [
          BottomNavigationBarItem(icon: Icon(Icons.home), label: 'QR code'),
          BottomNavigationBarItem(icon: Icon(Icons.message), label: 'Statistiques'),
          BottomNavigationBarItem(icon: Icon(Icons.add), label: 'Accueil'),
          BottomNavigationBarItem(icon: Icon(Icons.notifications), label: 'Fournisseurs'),
          BottomNavigationBarItem(icon: Icon(Icons.person), label: 'Clients'),
        ],
        selectedItemColor: AppColors.primaryColor,
        unselectedItemColor: AppColors.primaryColorDisable,
      ),
    );
  }
}
