// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables, library_private_types_in_public_api

import 'package:adisoft_mobile/pages/PagesClientsFournisseurs/FluxClients/PagesListe/listeDesClients.dart';
import 'package:adisoft_mobile/pages/PagesClientsFournisseurs/FluxClients/PagesListe/listeOffre.dart';
import 'package:adisoft_mobile/pages/pagesArticles/listeDesArticles.dart';
import 'package:flutter/material.dart';
import 'package:adisoft_mobile/FuntionsGlobal.dart';
import 'package:adisoft_mobile/globalColors.dart';

class pageClient extends StatefulWidget {
  const pageClient({Key? key}) : super(key: key);

  @override
  _pageClientState createState() => _pageClientState();
}

class _pageClientState extends State<pageClient> {
  int _selectedIndex = 4;

  void _navigateBottomBar(int index) {
    setState(() {
      _selectedIndex = index;
      footerNavigationSwitch(_selectedIndex, context);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Flux Client", style: TextStyle(color: AppColors.white)),
        centerTitle: true,
        backgroundColor: AppColors.primaryColor,
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        
        children: [
          // Buttons
          
          Padding(
            padding: EdgeInsets.all(5),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                OutlinedButton(
                  style: OutlinedButton.styleFrom(
                    minimumSize: Size(150, 40),
                    maximumSize: Size(200, 40),
                    side: BorderSide(
                      color: AppColors.primaryColor,
                    ),
                    textStyle: const TextStyle(fontSize: 18),
                    foregroundColor: AppColors.primaryColor,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(12),
                    ),
                  ),
                  onPressed: () {
                    Navigator.of(context).pushReplacement(
                      MaterialPageRoute(builder: (context) => pagelisteDesClients())
                    );
                  },
                  child: Text("Clients"),
                ),
                OutlinedButton(
                  style: OutlinedButton.styleFrom(
                    minimumSize: Size(150, 40),
                    maximumSize: Size(200, 40),
                    side: BorderSide(
                      color: AppColors.primaryColor,
                    ),
                    textStyle: const TextStyle(fontSize: 18),
                    foregroundColor: AppColors.primaryColor,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(12),
                    ),
                  ),
                  onPressed: () {
                    Navigator.of(context).pushReplacement(
                      MaterialPageRoute(builder: (context) => pageListeDeArticles())
                    );
                  },
                  child: Text("Articles"),
                ),
              ],
            ),
          ),
          // Big boxes with images and text
          Expanded(
            child: GridView.count(
              crossAxisCount: 2,
              mainAxisSpacing: 20,
              crossAxisSpacing: 20,
              padding: EdgeInsets.all(20),
              children: [
                buildBigBox('assets/images/offre.webp', 'Offre'),
                buildBigBox('assets/images/commandeClient.webp', 'Commande client'),
                buildBigBox('assets/images/noteEnvoi.webp', 'Note d''envoi'),
                buildBigBox('assets/images/vente.webp', 'Vente'),
                buildBigBox('assets/images/noteCreditVente.webp', 'Note de crédit sur vente'),
              ],
            ),
          ),
          //   SizedBox(height: 1),
          //   Expanded(
          //   child: GridView.count(
              
          //     crossAxisCount: 2,
          //     mainAxisSpacing: 20,
          //     crossAxisSpacing: 20,
          //     padding: EdgeInsets.all(20),
          //     children: [
          //       buildBigBox('assets/images/noteCreditVente.webp', 'Note de crédit sur vente'),
          //     ],
          //   ),
          // ),
        ],
      ),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _selectedIndex,
        onTap: _navigateBottomBar,
        type: BottomNavigationBarType.fixed,
        items: [
          BottomNavigationBarItem(icon: Icon(Icons.home), label: 'QR code'),
          BottomNavigationBarItem(icon: Icon(Icons.message), label: 'Statistiques'),
          BottomNavigationBarItem(icon: Icon(Icons.add), label: 'Accueil'),
          BottomNavigationBarItem(icon: Icon(Icons.notifications), label: 'Fournisseurs'),
          BottomNavigationBarItem(icon: Icon(Icons.person), label: 'Clients'),
        ],
        selectedItemColor: AppColors.primaryColor,
        unselectedItemColor: AppColors.primaryColorDisable,
      ),
    );
  }

Widget buildBigBox(String imagePath, String text) {
  return GestureDetector(
    onTap: () {
      // Execute different code based on the text
      switch (text) {
        case 'Offre':
          // Code for 'Offre'
          Navigator.of(context).pushReplacement(
            MaterialPageRoute(builder: (context) => pagelisteOffre()),
          );
          print('Handling tap for Offre');
          break;
        case 'Commande client':
          // Code for 'Commande client'
          print('Handling tap for Commande client');
          break;
        case 'Note d\'envoi':
          // Code for 'Note d\'envoi'
          print('Handling tap for Note d\'envoi');
          break;
        case 'Vente':
          // Code for 'Vente'
          print('Handling tap for Vente');
          break;
        case 'Note de crédit sur vente':
          // Code for 'Note de crédit sur vente'
          print('Handling tap for Note de crédit sur vente');
          break;
        default:
          print('No action defined for $text');
      }
    },
    child: Container(
        decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(20),
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.2),
            spreadRadius: 3,
            blurRadius: 10,
            offset: Offset(0, 3),
          ),
        ],
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 5.0), // Padding around the image
            child: Image.asset(
              imagePath,
              width: 75,
            ),
          ),
          SizedBox(height: 10),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0), // Padding around the text
            child: Text(
              text,
              textAlign: TextAlign.center, 
              style: TextStyle(
                fontSize: 18,
                height: 1.1,
              ),
            ),
          ),
        ],
      ),
    ),
  );
}
}
