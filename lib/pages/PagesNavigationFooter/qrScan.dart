// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables

import 'package:flutter/material.dart';
import 'package:adisoft_mobile/FuntionsGlobal.dart';
import 'package:adisoft_mobile/globalColors.dart';

class pageQRscan extends StatefulWidget {
  const pageQRscan({Key? key}) :super(key: key);

  @override
  _pageQRscanState createState() => _pageQRscanState();
}

class _pageQRscanState extends State<pageQRscan>{
  int _selectedIndex = 0;

  void _navigateBottomBar(int index){
    setState(() {
      _selectedIndex = index;
      footerNavigationSwitch(_selectedIndex, context);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: BackButton(
          color: AppColors.white,
        ),
      ),
      body: Center(
        child: Text(
          'QR scan',
          style: TextStyle(fontSize: 50),
        ),
      ),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _selectedIndex,
        onTap: _navigateBottomBar,
        type: BottomNavigationBarType.fixed,
        items: [
          BottomNavigationBarItem(icon: Icon(Icons.home), label: 'Home'),
          BottomNavigationBarItem(icon: Icon(Icons.message), label: 'Message'),
          BottomNavigationBarItem(icon: Icon(Icons.add), label: 'Add'),
          BottomNavigationBarItem(icon: Icon(Icons.notifications), label: 'Notifications'),
          BottomNavigationBarItem(icon: Icon(Icons.person), label: 'Person'),
        ],
        selectedItemColor:  AppColors.primaryColor, 
        unselectedItemColor: AppColors.primaryColorDisable, 
      ),
    );
  }
}

