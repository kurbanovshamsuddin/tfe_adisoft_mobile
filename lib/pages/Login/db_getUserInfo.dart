// PAGE QUI FAIT CONNECTION AU APP ET RECUPERE ID UTILISATEUR ET COOKIE DE CONNECTION POUR RECUPERER INFO DE UTILISATEUR PAR ID UTILISATEUR
// L'INFO UTILISER POUR COMPARER LOGIN AND MDP ENCODER PAR UTILISATEUR
// 
// EXEMPLE DE UTILISATION : Map<String, dynamic> userData = await getUserName(userid);

import 'dart:convert';
import 'package:adisoft_mobile/pages/Login/db_connection.dart';
import 'package:http/http.dart' as http;

Future<Map<String, dynamic>> getUserName() async {
  // API endpoint
  int id = LoginResponse.userId.toInt();
  final uri = Uri.parse('https://internaldemo.adisoft.app/ui/usermanagement/user/get/$id');
  Map<String, dynamic> jsonResponse = {};

  final response = await http.get(
    uri,
    headers: {
      'Content-Type': 'application/json', 
      'Referer': 'https://internaldemo.adisoft.app',
      'Cookie': LoginResponse.cookie,
    },
  );


  if (response.statusCode == 200) {
  // Parse the JSON response
  jsonResponse = json.decode(response.body);
  String firstName = jsonResponse['response']['firstName'];
  print('User firstName: $firstName');
  } else {
    // Handle error
    print('Error: ${response.statusCode}');
  }
  return jsonResponse;
}

