// PAGE QUI FAIT APPEL AU PAGES 'db_connection.dart' et 'db_getUserInfo' '
// L'INFO UTILISER POUR COMPARER LOGIN AND MDP ENCODER PAR UTILISATEUR
// 
// EXEMPLE DE UTILISATION : Map<String, dynamic> userData = await getUserName(userid);

import 'package:adisoft_mobile/pages/Login/db_getUserInfo.dart';
import 'package:adisoft_mobile/pages/PagesNavigationFooter/homepage.dart';
import 'package:flutter/material.dart';

import 'db_connection.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  TextEditingController _usernameController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();

  // Ajout d'une variable pour gérer le message d'état de la connexion
  String _loginStatus = '';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Page de Connexion'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            TextField(
              controller: _usernameController,
              decoration: InputDecoration(labelText: 'Nom d\'utilisateur'),
            ),
            SizedBox(height: 20),
            TextField(
              controller: _passwordController,
              obscureText: true,
              decoration: InputDecoration(labelText: 'Mot de passe'),
            ),
            SizedBox(height: 20),
            ElevatedButton(
              onPressed: () async {
                // logique de connexion ici
                String username = _usernameController.text;
                String password = _passwordController.text;
                // logique d'authentification ou appel API
                bool isSuccess = false;
                await userLogin(username, password);
                int id = LoginResponse.userId.toInt();
                // int userid = loginResponses.userId.toInt();
                if (id != 0){
                  Map<String, dynamic> userData = await getUserName();
                  isSuccess = userData['success'];
                }
                // Vérification de connection
                if (isSuccess == true) {
                  // La connexion est réussie
                  setState(() {
                    _loginStatus = 'Connexion réussie !';
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => HomePage()),
                    );

                  });
                } else {
                  // La connexion a échoué
                  setState(() {
                    // _loginStatus = 'Nom d\'utilisateur ou mot de passe incorrect.';
                     _loginStatus = 'login to DB responce = $id,  find user by id is successful = $isSuccess';
                  });
                }
              },
              child: Text('Connexion'),
            ),
            SizedBox(height: 20),
            // Afficher le message d'état de la connexion
            Text(
              _loginStatus,
              style: TextStyle(
                color: _loginStatus.contains('réussie') ? Colors.green : Colors.red,
                fontWeight: FontWeight.bold,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
