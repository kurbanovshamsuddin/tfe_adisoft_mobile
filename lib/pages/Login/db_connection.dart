// PAGE QUI FAIT CONNECTION AU APP ET RECUPERE ID UTILISATEUR ET COOKIE DE CONNECTION
// ID EST RENVOYER AVEC 'return LoginResponse(userId);'
// COOKIE EST UN VARIABLE 'GLOBAL' EN PEUT ACCEDER UTILISANT LoginResponse.cookie. IL FAUT JUSTE APPORTER CETTE PAGE AVEC IMPORT POUR UTILISER.
// EXEMPLE DE UTILISATION : final loginResponses = await userLogin(username, password) | ca va envoyer id dans loginreponses et cookie sarais utilisable partout dans projet; 

import 'dart:convert';
import 'package:http/http.dart' as http;

class LoginResponse {
  static String cookie = '';
  static double userId = 0;
}

Future<void> userLogin(String username, String password) async {
  final uri = Uri.parse('https://internaldemo.adisoft.app/ui/usermanagement/usersession/connect');
  double userId = 0;
  try {
    final response = await http.post(
      uri,
      headers: {
        'Content-Type': 'application/json',
        'Referer': 'https://internaldemo.adisoft.app',
      },
      body: jsonEncode({
        'login': username,
        'password': password,
      }),
    );

    LoginResponse.cookie = response.headers["set-cookie"] ?? '';

    if (response.statusCode == 200) {
      Map<String, dynamic> jsonResponse = json.decode(response.body);
      userId = jsonResponse['response']['userId'];
      LoginResponse.userId = userId;
    } else {
      print('Error: ${response.statusCode}');
    }
  } catch (e) {
    print('Exception during login: $e');
  }
}



