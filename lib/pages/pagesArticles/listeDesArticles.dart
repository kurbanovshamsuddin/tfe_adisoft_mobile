// ignore_for_file: prefer_const_literals_to_create_immutables, prefer_const_constructors

import 'dart:convert';
import 'package:adisoft_mobile/pages/Login/db_connection.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import 'package:adisoft_mobile/FuntionsGlobal.dart';
import 'package:adisoft_mobile/globalColors.dart';

class pageListeDeArticles extends StatefulWidget {
  const pageListeDeArticles({Key? key}) : super(key: key);

  @override
  _pageListeDeArticlesState createState() => _pageListeDeArticlesState();
}

class _pageListeDeArticlesState extends State<pageListeDeArticles> {
  int _selectedIndex = 0;
  List<Map<String, dynamic>> _tableData = [];

  @override
  void initState() {
    super.initState();
    fetchData();
  }

  Future<void> fetchData() async {

    // final uri = Uri.parse('http://10.0.2.2:8087/article/load-article-list');
    final uri = Uri.parse('https://internaldemo.adisoft.app/ui/businessmanagement/article/load-article-list');
    
    final response = await http.post(
      uri,
      headers: {
        'Content-Type': 'application/json',
        'Referer': 'https://internaldemo.adisoft.app',
        'Cookie': LoginResponse.cookie,
      },
      body: jsonEncode({
        'folderTenantId': 1,
      }),
    );

    if (response.statusCode == 200) {
  // Successful response
  final Map<String, dynamic> responseData = json.decode(utf8.decode(response.bodyBytes));
  final List<dynamic> responseList = responseData['response'];
  final List<Map<String, dynamic>> dataList = responseList.map((item) => item as Map<String, dynamic>).toList();
  setState(() {
    _tableData = dataList;
  });
} else {
  // Handle error
  print('Error: ${response.statusCode}');
}
  }

  void _navigateBottomBar(int index) {
    setState(() {
      _selectedIndex = index;
      footerNavigationSwitch(_selectedIndex, context);
    });
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        leading: BackButton(
          color: AppColors.white, //change your color here
        ),
        title: const Text("Articles", style: TextStyle(color: AppColors.white)),
        centerTitle: true,
        backgroundColor: AppColors.primaryColor,
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          SizedBox(height: 5),
          RichText(
            text: TextSpan(
              children: [
                TextSpan(
                  text: 'Liste des ',
                  style: TextStyle(
                    fontSize: 36,
                    color: AppColors.textColor,
                    height: 2,
                  ),
                ),
                TextSpan(
                  text: 'articles',
                  style: TextStyle(
                    fontSize: 36,
                    color: AppColors.primaryColor, 
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height: 10),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10),
            child: Row(
              children: [
                Expanded(
                  child: TextField(
                    decoration: InputDecoration(
                      contentPadding: EdgeInsets.symmetric(vertical: 10),
                      hintText: 'Rechercher',
                      prefixIcon: Icon(Icons.search),
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(12),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(12),
                        borderSide: BorderSide(width: 2, color: AppColors.primaryColor),
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(12),
                        borderSide: BorderSide(width: 2, color: Colors.grey),
                      ),
                    ),
                  ),
                ),
                SizedBox(width: 5),
                ElevatedButton.icon(
                  onPressed: () {
                    // Handle button press
                  },
                  icon: Icon(
                    Icons.add,
                    size: 32,
                    color: AppColors.white,
                  ),
                  label: Text(''),
                  style: ElevatedButton.styleFrom(
                    padding: EdgeInsets.symmetric(horizontal: 8, vertical: 8),
                    alignment: Alignment(1, 0),
                    backgroundColor: AppColors.primaryColor,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(12),
                    ),
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height: 20),
          Expanded(
            child: ListView.builder(
              padding: EdgeInsets.all(15),
              itemCount: _tableData.length,
              itemBuilder: (BuildContext context, int index) {
                return Container(
                  decoration: BoxDecoration(
                    border: Border(
                      top: BorderSide(color: Colors.grey.withOpacity(0.5)),
                    ),
                  ),
                  child: Row(
                    children: [
                      Expanded(
                        child: Card(
                          elevation: 0,
                          child: ListTile(
                            contentPadding: EdgeInsets.zero,
                            subtitle: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  '${_tableData[index]['ref']}',
                                  overflow: TextOverflow.ellipsis,
                                  maxLines: 1,
                                ),
                                Text(
                                  '${_tableData[index]['sellingPriceExclVAT']} €',
                                  overflow: TextOverflow.ellipsis,
                                  maxLines: 1,
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                      SizedBox(width: 5),
                      Expanded(
                        child: Card(
                          elevation: 0,
                          child: ListTile(
                            contentPadding: EdgeInsets.zero,
                            subtitle: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  '${_tableData[index]['mainLabel']}',
                                  overflow: TextOverflow.ellipsis,
                                  maxLines: 1,
                                ),
                                Text(
                                  '${_tableData[index]['familyLabel']}',
                                  overflow: TextOverflow.ellipsis,
                                  maxLines: 1,
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                );
              },
            ),
          ),
        ],
      ),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _selectedIndex,
        onTap: _navigateBottomBar,
        type: BottomNavigationBarType.fixed,
        items: [
          BottomNavigationBarItem(icon: Icon(Icons.home), label: 'QR code'),
          BottomNavigationBarItem(icon: Icon(Icons.message), label: 'Statistiques'),
          BottomNavigationBarItem(icon: Icon(Icons.add), label: 'Accueil'),
          BottomNavigationBarItem(icon: Icon(Icons.notifications), label: 'Fournisseurs'),
          BottomNavigationBarItem(icon: Icon(Icons.person), label: 'Clients'),
        ],
        selectedItemColor: AppColors.primaryColorDisable,
        unselectedItemColor: AppColors.primaryColorDisable,
      ),
    );
  }
}


















