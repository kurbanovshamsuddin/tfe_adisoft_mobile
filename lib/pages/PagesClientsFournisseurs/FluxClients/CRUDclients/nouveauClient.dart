
// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables, library_private_types_in_public_api

import 'dart:convert';

import 'package:adisoft_mobile/pages/Login/db_connection.dart';
import 'package:adisoft_mobile/pages/PagesClientsFournisseurs/FluxClients/PagesListe/listeDesClients.dart';
import 'package:flutter/material.dart';
import 'package:adisoft_mobile/FuntionsGlobal.dart';
import 'package:adisoft_mobile/globalColors.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;

class pageNouveauClient extends StatefulWidget {
  const pageNouveauClient({Key? key}) : super(key: key);

  @override
  _pageNouveauClientState createState() => _pageNouveauClientState();
}

class UpperCaseTextFormatter extends TextInputFormatter {
  @override
  TextEditingValue formatEditUpdate(TextEditingValue oldValue, TextEditingValue newValue) {
    return TextEditingValue(
      text: newValue.text.toUpperCase(),
      selection: newValue.selection,
    );
  }
}

  

class _pageNouveauClientState extends State<pageNouveauClient> {
int _selectedIndex = 4;
String numeroTVA = "";
int resultat = 0;
Color borderColor = AppColors.disabledTextColor;

//TODO changer getCoutryISO to post apres update de 12 mai, passer '+' dans body

Future getCoutryISO(String num) async {
  final uri = Uri.parse('https://internaldemo.adisoft.app/ui/commonservices/phone/get-country-iso?phoneNumber=$num');
  try {
    final response = await http.get(
      uri,
      headers: {
        'Content-Type': 'application/json',
        'Referer': 'https://internaldemo.adisoft.app',
        'Cookie': LoginResponse.cookie,
      },
    );
    if (response.statusCode == 200) {
      print("pays = $response");
       Map<String, dynamic> responseMap = json.decode(response.body);
      format(responseMap['response'], num);
    } else {
      print('Error: ${response.statusCode}');
    }
    return response;
  } catch (e) {
    print('Exception error: $e');
    return null;
  }
}
Future format(String typeNum, String num) async {
  final uri = Uri.parse('https://internaldemo.adisoft.app/ui/commonservices/phone/format');
  try {
    final response = await http.post(
      uri,
      headers: {
        'Content-Type': 'application/json',
        'Referer': 'https://internaldemo.adisoft.app',
        'Cookie': LoginResponse.cookie,
      },
      body: jsonEncode({
      'countryIso': typeNum,
      'phoneNumber': num,
    }),
    );
    if (response.statusCode == 200) {
      print("pays = $response");
      Map<String, dynamic> responseMap = json.decode(response.body);
      setState(() {
        phone.text = responseMap['response'];
      });
    } else {
      print('Error: ${response.statusCode}');
    }
    return response;
  } catch (e) {
    print('Exception error: $e');
    return null;
  }
}


  var vatNumber  = TextEditingController();
  var name  = TextEditingController();
  var mail  = TextEditingController();
  var phone  = TextEditingController();
  var street  = TextEditingController();
  var streetNum = TextEditingController();
  var postalBox  = TextEditingController();
  var postalCode   = TextEditingController();
  var city  = TextEditingController();
  var country   = TextEditingController();

  void _navigateBottomBar(int index) {
    setState(() {
      _selectedIndex = index;
      footerNavigationSwitch(_selectedIndex, context);
    });
  }

@override
Widget build(BuildContext context) {
  return Scaffold(
    appBar: AppBar(
      leading: BackButton(
        color: AppColors.white,
      ),
      title: const Text("", style: TextStyle(color: Color.fromARGB(255, 17, 16, 16))),
      centerTitle: true,
      backgroundColor: AppColors.primaryColor,
    ),
    body: Column(
      children: [
        // Fixed content at the top
        Padding(
          padding: const EdgeInsets.all(10),
          child: Center(
            child: RichText(
              text: TextSpan(
                children: [
                  TextSpan(
                    text: 'Nouveau ',
                    style: const TextStyle(
                      fontSize: 30,
                      color: AppColors.textColor,
                    ),
                  ),
                  TextSpan(
                    text: 'client',
                    style: const TextStyle(
                      fontSize: 30,
                      color: AppColors.primaryColor,
                    ),
                  ),
                ],
              ),
              overflow: TextOverflow.ellipsis,
            ),
          ),
        ),
        // Scrollable content
         Expanded(
          child: SingleChildScrollView(
            child: Padding(
              padding: EdgeInsets.symmetric(vertical: 20,horizontal: 20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  TextField(
                      inputFormatters: [
                      UpperCaseTextFormatter(),
                      LengthLimitingTextInputFormatter(12),
                    ],
                    onChanged: (value) {
                    // fetchData(value);
                    value.toUpperCase();
                    setState(() {
                      numeroTVA = value;
                      if (numeroTVA.isNotEmpty) {
                        resultat = calculerCleTVA(numeroTVA.substring(2));
                        if (resultat == int.parse(numeroTVA.substring(numeroTVA.length - 5))){
                          borderColor =  AppColors.disabledTextColor;
                        }else{
                          borderColor =  AppColors.red;
                        }
                      } else {
                        resultat = 0;
                      }
                    });
                  },
                    controller: vatNumber,
                    style: TextStyle(color: borderColor),
                    decoration: InputDecoration(
                      hintText: 'Numéro de TVA',
                      contentPadding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
                      border: OutlineInputBorder( // Customize the border
                      borderRadius: BorderRadius.all(Radius.circular(4)),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: AppColors.primaryColor, width: 2.0), // Border color when focused
                    ),
                    
                    ),
                  ),

                  SizedBox(height: 10),
                  TextField(
                    controller: name,
                      style: TextStyle(color: AppColors.disabledTextColor),
                      decoration: InputDecoration(
                      hintText: 'Nom',
                      contentPadding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
                      border: OutlineInputBorder( // Customize the border
                      borderRadius: BorderRadius.all(Radius.circular(4)),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: AppColors.primaryColor, width: 2.0), // Border color when focused
                    ),
                    ),
                  ),
                  SizedBox(height: 10),
                  TextField(
                    controller: mail,
                      style: TextStyle(color: AppColors.disabledTextColor),
                      decoration: InputDecoration(
                      hintText: 'Mail',
                      contentPadding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
                      border: OutlineInputBorder( // Customize the border
                      borderRadius: BorderRadius.all(Radius.circular(4)),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: AppColors.primaryColor, width: 2.0), // Border color when focused
                    ),
                    ),
                  ),
                  SizedBox(height: 10),
                  Focus(
                    onFocusChange: (hasFocus) {
                      if (!hasFocus) {
                        // Call getCountryISO when focus is lost
                        getCoutryISO(phone.text.replaceAll(' ', ''));
                      }
                    },
                    child: TextField(
                      controller: phone,
                      keyboardType: TextInputType.phone,
                      inputFormatters: [
                       FilteringTextInputFormatter.allow(RegExp(r'^[()+\d -]{1,15}')),
                      ],
                      style: TextStyle(color: AppColors.disabledTextColor),
                      decoration: InputDecoration(
                        hintText: 'Numéro Téléphone',
                        contentPadding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(4)),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: AppColors.primaryColor, width: 2.0),
                        ),
                      ),
                      
                    ),
                  ),
                  SizedBox(height: 10),
                  Center( 
                  child: Text(
                    "Adresse",
                    style: TextStyle(
                      fontSize: 30,
                      color: AppColors.primaryColor,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                ),
                  SizedBox(height: 10),
                  TextField(
                    controller: street,
                    style: TextStyle(color: AppColors.disabledTextColor),
                    decoration: InputDecoration(
                      hintText: 'Rue',
                      contentPadding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
                      border: OutlineInputBorder( // Customize the border
                      borderRadius: BorderRadius.all(Radius.circular(4)),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: AppColors.primaryColor, width: 2.0), // Border color when focused
                    ),
                    ),
                  ),
                  SizedBox(height: 10),
                  Row(
                    children: [
                      Expanded(
                        child: TextField(
                          controller: streetNum ,
                          style: TextStyle(color: AppColors.disabledTextColor),
                          decoration: InputDecoration(
                            hintText: 'Numéro',
                            contentPadding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
                      border: OutlineInputBorder( // Customize the border
                      borderRadius: BorderRadius.all(Radius.circular(4)),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: AppColors.primaryColor, width: 2.0), // Border color when focused
                    ),
                          ),
                        ),
                      ),
                      SizedBox(width: 10),
                      Expanded(
                        child: TextField(
                          controller: postalBox,
                          style: TextStyle(color: AppColors.disabledTextColor),
                          decoration: InputDecoration(
                            hintText: 'Boîte postal',
                            contentPadding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
                      border: OutlineInputBorder( // Customize the border
                      borderRadius: BorderRadius.all(Radius.circular(4)),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: AppColors.primaryColor, width: 2.0), // Border color when focused
                    ),
                          ),
                        ),
                      ),
                    ],
                  ),

                  SizedBox(height: 10),
                  TextField(
                    controller: postalCode,
                    style: TextStyle(color: AppColors.disabledTextColor),
                    decoration: InputDecoration(
                      hintText: 'Code postal',
                      contentPadding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
                      border: OutlineInputBorder( // Customize the border
                      borderRadius: BorderRadius.all(Radius.circular(4)),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: AppColors.primaryColor, width: 2.0), // Border color when focused
                    ),
                    ),
                  ),
                  SizedBox(height: 10),
                  TextField(
                    controller: city,
                    style: TextStyle(color: AppColors.disabledTextColor),
                    decoration: InputDecoration(
                      hintText: 'Ville',
                      contentPadding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
                      border: OutlineInputBorder( // Customize the border
                      borderRadius: BorderRadius.all(Radius.circular(4)),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: AppColors.primaryColor, width: 2.0), // Border color when focused
                    ),
                    ),
                  ),
                  SizedBox(height: 10),
                  TextField(
                    controller: country,
                    style: TextStyle(color: AppColors.disabledTextColor),
                    decoration: InputDecoration(
                      hintText: 'Pays',
                      contentPadding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
                      border: OutlineInputBorder( // Customize the border
                      borderRadius: BorderRadius.all(Radius.circular(4)),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: AppColors.primaryColor, width: 2.0), // Border color when focused
                    ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
Container(
  padding: EdgeInsets.symmetric(vertical: 10,horizontal: 10), // 10 padding on each side
  child: Row(
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    crossAxisAlignment: CrossAxisAlignment.center,
    children: [
      Expanded(
        child: ElevatedButton(
          onPressed: () {
          Future addUser() async {
            final uri = Uri.parse('https://internaldemo.adisoft.app/ui/signaletique/thirdparty/create-third-party-customer?folderTenantId=1');
            try {
              final response = await http.post(
                uri,
                headers: {
                  'Content-Type': 'application/json',
                  'Referer': 'https://internaldemo.adisoft.app',
                  'Cookie': LoginResponse.cookie,
                },
                body: jsonEncode({
                  'primaryName': name.text,
                  'vatNumber': vatNumber.text,
                  'generalMail': mail.text,//
                  'generalPhone': phone.text,//
                  'street': street.text,
                  'streetNum': streetNum.text,//
                  'postalBox': postalBox.text,//
                  'postalCode': postalCode.text,//
                  'city': city.text,
                  'country': country.text,
                }),
              );
              if (response.statusCode == 200) {
                print("insert succesull");
                Navigator.of(context).pushReplacement(
                  MaterialPageRoute(builder: (context) => pagelisteDesClients()),
                );
                // Map<String, dynamic> jsonResponse = json.decode(response.body);
                // userId = jsonResponse['response']['userId'];
              } else {
                print('Error: ${response.statusCode}');
              }
              
              return null;
              
            } catch (e) {
              print('Exception error: $e');
              return null;
            }
          }

          addUser();

          },
          style: ElevatedButton.styleFrom(
            backgroundColor: AppColors.primaryColor, 
            foregroundColor: AppColors.white, 
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(4), 
            ),
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text('Confirmer  '),
              Icon(Icons.check),
            ],
          ),
        ),
      ),
    ],
  ),
),

      ],
    ),
    bottomNavigationBar: BottomNavigationBar(
      currentIndex: _selectedIndex,
      onTap: _navigateBottomBar,
      type: BottomNavigationBarType.fixed,
      items: const [
        BottomNavigationBarItem(icon: Icon(Icons.home), label: 'QR code'),
        BottomNavigationBarItem(icon: Icon(Icons.message), label: 'Statistiques'),
        BottomNavigationBarItem(icon: Icon(Icons.add), label: 'Accueil'),
        BottomNavigationBarItem(icon: Icon(Icons.notifications), label: 'Fournisseurs'),
        BottomNavigationBarItem(icon: Icon(Icons.person), label: 'Clients'),
      ],
      selectedItemColor: AppColors.primaryColor,
      unselectedItemColor: AppColors.primaryColorDisable,
    ),
  );
}

}
