// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables

import 'package:adisoft_mobile/pages/PagesClientsFournisseurs/FluxClients/CRUDclients/modificationClient.dart';
import 'package:adisoft_mobile/pages/PagesContact/listeDesContact.dart';
import 'package:flutter/material.dart';
import 'package:adisoft_mobile/FuntionsGlobal.dart';
import 'package:adisoft_mobile/globalColors.dart';

class pageafficheClient extends StatefulWidget {
  final Map<String, dynamic> selectedClient;
  const pageafficheClient({Key? key, required this.selectedClient}) : super(key: key);
  @override
  _pageafficheClientState createState() => _pageafficheClientState();
}

class _pageafficheClientState extends State<pageafficheClient> {
  int _selectedIndex = 4;

  late TextEditingController _nomController;
  late TextEditingController _numeroTVAController;
  late TextEditingController _mailController;
  late TextEditingController _phoneController;
  late TextEditingController _rueController;
  late TextEditingController _numeroController;
  late TextEditingController _BPController;
  late TextEditingController _CPController;
  late TextEditingController _villeController;
  late TextEditingController _paysController;

  
  @override
  void initState() {
    super.initState();
    _nomController = TextEditingController(text: widget.selectedClient['primaryName']);
    _numeroTVAController = TextEditingController(text: widget.selectedClient['vatNumber']);
    _mailController = TextEditingController(text: widget.selectedClient['generalMail']);
    _phoneController = TextEditingController(text: widget.selectedClient['generalPhone']);
    _rueController = TextEditingController(text: widget.selectedClient['street']);
    _numeroController = TextEditingController(text: widget.selectedClient['streetNum']);
    _BPController = TextEditingController(text: widget.selectedClient['postalBox']);
    _CPController = TextEditingController(text: widget.selectedClient['postalCode']);
    _villeController = TextEditingController(text: widget.selectedClient['city']);
    _paysController = TextEditingController(text: widget.selectedClient['country']);
  }

  void _navigateBottomBar(int index) {
    setState(() {
      _selectedIndex = index;
      footerNavigationSwitch(_selectedIndex, context);
    });
  }


@override
Widget build(BuildContext context) {
  return Scaffold(
    appBar: AppBar(
      leading: BackButton(
        color: AppColors.white,
      ),
      title: const Text("", style: TextStyle(color: Color.fromARGB(255, 17, 16, 16))),
      centerTitle: true,
      backgroundColor: AppColors.primaryColor,
    ),
    body: Column(
      children: [
        // Fixed content at the top
        Padding(
          padding: const EdgeInsets.all(10),
          child: Center(
            child: RichText(
              text: TextSpan(
                children: [
                  TextSpan(
                    text: widget.selectedClient['primaryName'],
                    style: const TextStyle(
                      fontSize: 30,
                      color: AppColors.primaryColor, 
                    ),
                  ),
                ],
              ),
              overflow: TextOverflow.ellipsis,
            ),
          ),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            ElevatedButton(
              onPressed: () {
                Navigator.of(context).push(
                  MaterialPageRoute(builder: (context) => pagelisteDesContacts(selectedContact: widget.selectedClient))
                );
              },
              style: ElevatedButton.styleFrom(
                backgroundColor: AppColors.primaryColor,
                foregroundColor: AppColors.white,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(4),
                ),
              ),
              child: const Row(
                children: [
                  Text('Contacts   '),
                  Icon(Icons.contacts),
                ],
              ),
            ),
            ElevatedButton(
              onPressed: () {
                Navigator.of(context).push(
                  MaterialPageRoute(builder: (context) => pageModificationClient(selectedClient: widget.selectedClient))
                );
              },
              style: ElevatedButton.styleFrom(
                backgroundColor: AppColors.primaryColor, 
                foregroundColor: AppColors.white, 
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(4), 
                ),
              ),
              child: Row(
                children: [
                  Text('Modifier   '),
                  Icon(Icons.edit),
                ],
              ),
            ),
          ],
        ),
        // Scrollable content
         Expanded(
          child: SingleChildScrollView(
            child: Padding(
              padding: EdgeInsets.symmetric(vertical: 20,horizontal: 20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  TextField(
                    controller: _numeroTVAController,
                    enabled: false,
                    style: TextStyle(color: AppColors.disabledTextColor),
                    decoration: InputDecoration(
                      hintText: 'Numéro de TVA',
                      contentPadding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
                      border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(4)),
                    ),
                    filled: true, 
                    fillColor:AppColors.FlutterDefautBackground, 
                    hintStyle: TextStyle(color: AppColors.disabledTextColor), 
                    disabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: AppColors.disabledTextColor), 
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: AppColors.primaryColor, width: 2.0), 
                    ),
                    ),
                    
                  ),
                  SizedBox(height: 10),
                  TextField(
                     controller: _nomController,
                     enabled: false,
                      style: TextStyle(color: AppColors.disabledTextColor),
                    decoration: InputDecoration(
                      hintText: 'Nom',
                      contentPadding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
                      border: OutlineInputBorder( 
                      borderRadius: BorderRadius.all(Radius.circular(4)),
                    ),
                    filled: true, // Add background color
                    fillColor:AppColors.FlutterDefautBackground, 
                    hintStyle: TextStyle(color: AppColors.disabledTextColor), 
                    disabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: AppColors.disabledTextColor), 
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: AppColors.primaryColor, width: 2.0), 
                    ),
                    ),
                  ),
                  SizedBox(height: 10),
                  TextField(
                      controller: _mailController,
                      enabled: false,
                      style: TextStyle(color: AppColors.disabledTextColor),
                      decoration: InputDecoration(
                      hintText: 'Mail',
                      contentPadding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
                      border: OutlineInputBorder( 
                      borderRadius: BorderRadius.all(Radius.circular(4)),
                    ),
                    filled: true, 
                    fillColor:AppColors.FlutterDefautBackground,
                    hintStyle: TextStyle(color: AppColors.disabledTextColor), 
                    disabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: AppColors.disabledTextColor), 
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: AppColors.primaryColor, width: 2.0), 
                    ),
                    ),
                  ),
                  SizedBox(height: 10),
                  TextField(
                    controller: _phoneController,
                    enabled: false,
                    style: TextStyle(color: AppColors.disabledTextColor),
                    decoration: InputDecoration(
                      hintText: 'Numéro Téléphone',
                      contentPadding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
                      border: OutlineInputBorder( 
                      borderRadius: BorderRadius.all(Radius.circular(4)),
                    ),
                    filled: true, 
                    fillColor:AppColors.FlutterDefautBackground, 
                    hintStyle: TextStyle(color: AppColors.disabledTextColor), 
                    disabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: AppColors.disabledTextColor), 
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: AppColors.primaryColor, width: 2.0), 
                    ),
                    ),
                  ),
                  SizedBox(height: 10),
                  Center( 
                  child: Text(
                    "Adresse",
                    style: TextStyle(
                      fontSize: 30,
                      color: AppColors.primaryColor,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                ),
                  SizedBox(height: 10),
                  TextField(
                    controller: _rueController,
                    enabled: false,
                    style: TextStyle(color: AppColors.disabledTextColor),
                    decoration: InputDecoration(
                      hintText: 'Rue',
                      contentPadding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
                      border: OutlineInputBorder( 
                      borderRadius: BorderRadius.all(Radius.circular(4)),
                    ),

                    filled: true, 
                    fillColor:AppColors.FlutterDefautBackground, 
                    hintStyle: TextStyle(color: AppColors.disabledTextColor),
                    disabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: AppColors.disabledTextColor),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: AppColors.primaryColor, width: 2.0),
                    ),
                    ),
                  ),
                  SizedBox(height: 10),
                  Row(
                    children: [
                      Expanded(
                        child: TextField(
                          controller:  _numeroController,
                          enabled: false,
                          style: TextStyle(color: AppColors.disabledTextColor),
                          decoration: InputDecoration(
                            hintText: 'Numéro',
                            contentPadding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
                      border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(4)),
                    ),
                    filled: true,
                    fillColor:AppColors.FlutterDefautBackground,
                    hintStyle: TextStyle(color: AppColors.disabledTextColor),
                    disabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: AppColors.disabledTextColor),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: AppColors.primaryColor, width: 2.0),
                    ),
                          ),
                        ),
                      ),
                      SizedBox(width: 10),
                      Expanded(
                        child: TextField(
                          controller: _BPController,
                          enabled: false,
                          style: TextStyle(color: AppColors.disabledTextColor),
                          decoration: InputDecoration(
                            hintText: 'Boîte postal',
                            contentPadding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
                      border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(4)),
                    ),
                    filled: true,
                    fillColor:AppColors.FlutterDefautBackground,
                    hintStyle: TextStyle(color: AppColors.disabledTextColor),
                    disabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: AppColors.disabledTextColor),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: AppColors.primaryColor, width: 2.0),
                    ),
                          ),
                        ),
                      ),
                    ],
                  ),

                  SizedBox(height: 10),
                  TextField(
                    controller: _CPController,
                    enabled: false,
                    style: TextStyle(color: AppColors.disabledTextColor),
                    decoration: InputDecoration(
                      hintText: 'Code postal',
                      contentPadding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
                      border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(4)),
                    ),
                    filled: true,
                    fillColor:AppColors.FlutterDefautBackground,
                    hintStyle: TextStyle(color: AppColors.disabledTextColor),
                    disabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: AppColors.disabledTextColor),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: AppColors.primaryColor, width: 2.0),
                    ),
                    ),
                  ),
                  SizedBox(height: 10),
                  TextField(
                    controller: _villeController,
                    enabled: false,
                    style: TextStyle(color: AppColors.disabledTextColor),
                    decoration: InputDecoration(
                      hintText: 'Ville',
                      contentPadding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
                      border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(4)),
                    ),
                    filled: true,
                    fillColor:AppColors.FlutterDefautBackground,
                    hintStyle: TextStyle(color: AppColors.disabledTextColor),
                    disabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: AppColors.disabledTextColor),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: AppColors.primaryColor, width: 2.0),
                    ),
                    ),
                  ),
                  SizedBox(height: 10),
                  TextField(
                    controller: _paysController,
                    enabled: false,
                    style: TextStyle(color: AppColors.disabledTextColor),
                    decoration: InputDecoration(
                      hintText: 'Pays',
                      contentPadding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
                      border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(4)),
                    ),
                    filled: true,
                    fillColor:AppColors.FlutterDefautBackground,
                    hintStyle: TextStyle(color: AppColors.disabledTextColor),
                    disabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: AppColors.disabledTextColor),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: AppColors.primaryColor, width: 2.0),
                    ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ],
    ),
    bottomNavigationBar: BottomNavigationBar(
      currentIndex: _selectedIndex,
      onTap: _navigateBottomBar,
      type: BottomNavigationBarType.fixed,
      items: const [
        BottomNavigationBarItem(icon: Icon(Icons.home), label: 'QR code'),
        BottomNavigationBarItem(icon: Icon(Icons.message), label: 'Statistiques'),
        BottomNavigationBarItem(icon: Icon(Icons.add), label: 'Accueil'),
        BottomNavigationBarItem(icon: Icon(Icons.notifications), label: 'Fournisseurs'),
        BottomNavigationBarItem(icon: Icon(Icons.person), label: 'Clients'),
      ],
      selectedItemColor: AppColors.primaryColor,
      unselectedItemColor: AppColors.primaryColorDisable,
    ),
  );
}

}
