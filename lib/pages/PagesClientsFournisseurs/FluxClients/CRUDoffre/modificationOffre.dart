// // ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables, library_private_types_in_public_api

//////////////////////////////////////////////////CODE DE MODIF CLIENT A ADAPTER POUR OFFRE

// import 'dart:convert';

// import 'package:adisoft_mobile/pages/Login/db_connection.dart';
// import 'package:adisoft_mobile/pages/PagesClientsFournisseurs/FluxClients/PagesListe/listeDesClients.dart';
// import 'package:flutter/material.dart';
// import 'package:adisoft_mobile/FuntionsGlobal.dart';
// import 'package:adisoft_mobile/globalColors.dart';
// import 'package:flutter/services.dart';
// import 'package:http/http.dart' as http;

// class pageModificationClient extends StatefulWidget {
//   final Map<String, dynamic> selectedClient;
//   const pageModificationClient({Key? key, required this.selectedClient}) : super(key: key);

//   @override
//   _pageModificationClientState createState() => _pageModificationClientState();
// }

// class _pageModificationClientState extends State<pageModificationClient> {
//   int _selectedIndex = 4;

//   late TextEditingController _nomController;
//   late TextEditingController _numeroTVAController;
//   late TextEditingController _mailController;
//   late TextEditingController _phoneController;
//   late TextEditingController _rueController;
//   late TextEditingController _numeroController;
//   late TextEditingController _BPController;
//   late TextEditingController _CPController;
//   late TextEditingController _villeController;
//   late TextEditingController _paysController;

  
//   @override
//   void initState() {
//     super.initState();
//     _nomController = TextEditingController(text: widget.selectedClient['primaryName']);
//     _numeroTVAController = TextEditingController(text: widget.selectedClient['vatNumber']);
//     _mailController = TextEditingController(text: widget.selectedClient['generalMail']);
//     _phoneController = TextEditingController(text: widget.selectedClient['generalPhone']);
//     _rueController = TextEditingController(text: widget.selectedClient['street']);
//     _numeroController = TextEditingController(text: widget.selectedClient['streetNum']);
//     _BPController = TextEditingController(text: widget.selectedClient['postalBox']);
//     _CPController = TextEditingController(text: widget.selectedClient['postalCode']);
//     _villeController = TextEditingController(text: widget.selectedClient['city']);
//     _paysController = TextEditingController(text: widget.selectedClient['country']);
//   }

// Future getCoutryISO(String num) async {
//   final uri = Uri.parse('https://internaldemo.adisoft.app/ui/commonservices/phone/get-country-iso?phoneNumber=$num');
//   try {
//     final response = await http.get(
//       uri,
//       headers: {
//         'Content-Type': 'application/json',
//         'Referer': 'https://internaldemo.adisoft.app',
//         'Cookie': LoginResponse.cookie,
//       },
//     );
//     if (response.statusCode == 200) {
//       print("pays = $response");
//        Map<String, dynamic> responseMap = json.decode(response.body);
//       format(responseMap['response'], num);
//     } else {
//       print('Error: ${response.statusCode}');
//     }
//     return response;
//   } catch (e) {
//     print('Exception error: $e');
//     return null;
//   }
// }
// Future format(String typeNum, String num) async {
//   final uri = Uri.parse('https://internaldemo.adisoft.app/ui/commonservices/phone/format');
//   try {
//     final response = await http.post(
//       uri,
//       headers: {
//         'Content-Type': 'application/json',
//         'Referer': 'https://internaldemo.adisoft.app',
//         'Cookie': LoginResponse.cookie,
//       },
//       body: jsonEncode({
//       'countryIso': typeNum,
//       'phoneNumber': num,
//     }),
//     );
//     if (response.statusCode == 200) {
//       print("pays = $response");
//       Map<String, dynamic> responseMap = json.decode(response.body);
//       setState(() {
//         _phoneController.text = responseMap['response'];
//       });
//     } else {
//       print('Error: ${response.statusCode}');
//     }
//     return response;
//   } catch (e) {
//     print('Exception error: $e');
//     return null;
//   }
// }
//   void _navigateBottomBar(int index) {
//     setState(() {
//       _selectedIndex = index;
//       footerNavigationSwitch(_selectedIndex, context);
//     });
//   }

// @override
// Widget build(BuildContext context) {
//   return Scaffold(
//     appBar: AppBar(
//       leading: BackButton(
//         color: AppColors.white,
//       ),
//       title: const Text("", style: TextStyle(color: Color.fromARGB(255, 17, 16, 16))),
//       centerTitle: true,
//       backgroundColor: AppColors.primaryColor,
//     ),
//     body: Column(
//       children: [
//         // Fixed content at the top
//         Padding(
//           padding: const EdgeInsets.all(10),
//           child: Center(
//             child: RichText(
//               text: TextSpan(
//                 children: [
//                   TextSpan(
//                     text: 'modification d\'un ',
//                     style: const TextStyle(
//                       fontSize: 30,
//                       color: AppColors.textColor,
//                     ),
//                   ),
//                   TextSpan(
//                     text: 'client',
//                     style: const TextStyle(
//                       fontSize: 30,
//                       color: AppColors.primaryColor,
//                     ),
//                   ),
//                 ],
//               ),
//               overflow: TextOverflow.ellipsis,
//             ),
//           ),
//         ),
//         // Scrollable content
//          Expanded(
//           child: SingleChildScrollView(
//             child: Padding(
//               padding: EdgeInsets.symmetric(vertical: 20,horizontal: 20),
//               child: Column(
//                 crossAxisAlignment: CrossAxisAlignment.start,
//                 children: [
//                   TextField(
//                     controller: _numeroTVAController,
//                     style: TextStyle(color: AppColors.textColor),
//                     decoration: InputDecoration(
//                       hintText: 'Numéro de TVA',
//                       contentPadding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
//                       border: OutlineInputBorder(
//                       borderRadius: BorderRadius.all(Radius.circular(4)),
//                     ),
//                     filled: true, 
//                     fillColor:AppColors.FlutterDefautBackground, 
//                     hintStyle: TextStyle(color: AppColors.textColor), 
//                     disabledBorder: OutlineInputBorder(
//                       borderSide: BorderSide(color: AppColors.textColor), 
//                     ),
//                     focusedBorder: OutlineInputBorder(
//                       borderSide: BorderSide(color: AppColors.primaryColor, width: 2.0), 
//                     ),
//                     ),
                    
//                   ),
//                   SizedBox(height: 10),
//                   TextField(
//                      controller: _nomController,
//                       style: TextStyle(color: AppColors.textColor),
//                     decoration: InputDecoration(
//                       hintText: 'Nom',
//                       contentPadding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
//                       border: OutlineInputBorder( 
//                       borderRadius: BorderRadius.all(Radius.circular(4)),
//                     ),
//                     filled: true, // Add background color
//                     fillColor:AppColors.FlutterDefautBackground, 
//                     hintStyle: TextStyle(color: AppColors.textColor), 
//                     disabledBorder: OutlineInputBorder(
//                       borderSide: BorderSide(color: AppColors.textColor), 
//                     ),
//                     focusedBorder: OutlineInputBorder(
//                       borderSide: BorderSide(color: AppColors.primaryColor, width: 2.0), 
//                     ),
//                     ),
//                   ),
//                   SizedBox(height: 10),
//                   TextField(
//                       controller: _mailController,
//                       style: TextStyle(color: AppColors.textColor),
//                       decoration: InputDecoration(
//                       hintText: 'Mail',
//                       contentPadding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
//                       border: OutlineInputBorder( 
//                       borderRadius: BorderRadius.all(Radius.circular(4)),
//                     ),
//                     filled: true, 
//                     fillColor:AppColors.FlutterDefautBackground,
//                     hintStyle: TextStyle(color: AppColors.textColor), 
//                     disabledBorder: OutlineInputBorder(
//                       borderSide: BorderSide(color: AppColors.textColor), 
//                     ),
//                     focusedBorder: OutlineInputBorder(
//                       borderSide: BorderSide(color: AppColors.primaryColor, width: 2.0), 
//                     ),
//                     ),
//                   ),
//                   SizedBox(height: 10),
//                   Focus(
//                     onFocusChange: (hasFocus) {
//                       if (!hasFocus) {
//                         // Call getCountryISO when focus is lost
//                         getCoutryISO(_phoneController.text.replaceAll(' ', ''));
//                       }
//                     },
//                     child: TextField(
//                       controller: _phoneController,
//                       keyboardType: TextInputType.phone,
//                       inputFormatters: [
//                        FilteringTextInputFormatter.allow(RegExp(r'^[()+\d -]{1,15}')),
//                       ],
//                       style: TextStyle(color: AppColors.disabledTextColor),
//                       decoration: InputDecoration(
//                         hintText: 'Numéro Téléphone',
//                         contentPadding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
//                         border: OutlineInputBorder(
//                           borderRadius: BorderRadius.all(Radius.circular(4)),
//                         ),
//                         focusedBorder: OutlineInputBorder(
//                           borderSide: BorderSide(color: AppColors.primaryColor, width: 2.0),
//                         ),
//                       ),
                      
//                     ),
//                   ),
//                   // TextField(
//                   //   controller: _phoneController,
//                   //   style: TextStyle(color: AppColors.textColor),
//                   //   decoration: InputDecoration(
//                   //     hintText: 'Numéro Téléphone',
//                   //     contentPadding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
//                   //     border: OutlineInputBorder( 
//                   //     borderRadius: BorderRadius.all(Radius.circular(4)),
//                   //   ),
//                   //   filled: true, 
//                   //   fillColor:AppColors.FlutterDefautBackground, 
//                   //   hintStyle: TextStyle(color: AppColors.textColor), 
//                   //   disabledBorder: OutlineInputBorder(
//                   //     borderSide: BorderSide(color: AppColors.textColor), 
//                   //   ),
//                   //   focusedBorder: OutlineInputBorder(
//                   //     borderSide: BorderSide(color: AppColors.primaryColor, width: 2.0), 
//                   //   ),
//                   //   ),
//                   // ),
//                   SizedBox(height: 10),
//                   Center( 
//                   child: Text(
//                     "Adresse",
//                     style: TextStyle(
//                       fontSize: 30,
//                       color: AppColors.primaryColor,
//                       fontWeight: FontWeight.w500,
//                     ),
//                   ),
//                 ),
//                   SizedBox(height: 10),
//                   TextField(
//                     controller: _rueController,
//                     style: TextStyle(color: AppColors.textColor),
//                     decoration: InputDecoration(
//                       hintText: 'Rue',
//                       contentPadding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
//                       border: OutlineInputBorder( 
//                       borderRadius: BorderRadius.all(Radius.circular(4)),
//                     ),

//                     filled: true, 
//                     fillColor:AppColors.FlutterDefautBackground, 
//                     hintStyle: TextStyle(color: AppColors.textColor),
//                     disabledBorder: OutlineInputBorder(
//                       borderSide: BorderSide(color: AppColors.textColor),
//                     ),
//                     focusedBorder: OutlineInputBorder(
//                       borderSide: BorderSide(color: AppColors.primaryColor, width: 2.0),
//                     ),
//                     ),
//                   ),
//                   SizedBox(height: 10),
//                   Row(
//                     children: [
//                       Expanded(
//                         child: TextField(
//                           controller:  _numeroController,
//                           style: TextStyle(color: AppColors.textColor),
//                           decoration: InputDecoration(
//                             hintText: 'Numéro',
//                             contentPadding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
//                       border: OutlineInputBorder(
//                       borderRadius: BorderRadius.all(Radius.circular(4)),
//                     ),
//                     filled: true,
//                     fillColor:AppColors.FlutterDefautBackground,
//                     hintStyle: TextStyle(color: AppColors.textColor),
//                     disabledBorder: OutlineInputBorder(
//                       borderSide: BorderSide(color: AppColors.textColor),
//                     ),
//                     focusedBorder: OutlineInputBorder(
//                       borderSide: BorderSide(color: AppColors.primaryColor, width: 2.0),
//                     ),
//                           ),
//                         ),
//                       ),
//                       SizedBox(width: 10),
//                       Expanded(
//                         child: TextField(
//                           controller: _BPController,
//                           style: TextStyle(color: AppColors.textColor),
//                           decoration: InputDecoration(
//                             hintText: 'Boîte postal',
//                             contentPadding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
//                       border: OutlineInputBorder(
//                       borderRadius: BorderRadius.all(Radius.circular(4)),
//                     ),
//                     filled: true,
//                     fillColor:AppColors.FlutterDefautBackground,
//                     hintStyle: TextStyle(color: AppColors.textColor),
//                     disabledBorder: OutlineInputBorder(
//                       borderSide: BorderSide(color: AppColors.textColor),
//                     ),
//                     focusedBorder: OutlineInputBorder(
//                       borderSide: BorderSide(color: AppColors.primaryColor, width: 2.0),
//                     ),
//                           ),
//                         ),
//                       ),
//                     ],
//                   ),

//                   SizedBox(height: 10),
//                   TextField(
//                     controller: _CPController,
//                     style: TextStyle(color: AppColors.textColor),
//                     decoration: InputDecoration(
//                       hintText: 'Code postal',
//                       contentPadding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
//                       border: OutlineInputBorder(
//                       borderRadius: BorderRadius.all(Radius.circular(4)),
//                     ),
//                     filled: true,
//                     fillColor:AppColors.FlutterDefautBackground,
//                     hintStyle: TextStyle(color: AppColors.textColor),
//                     disabledBorder: OutlineInputBorder(
//                       borderSide: BorderSide(color: AppColors.textColor),
//                     ),
//                     focusedBorder: OutlineInputBorder(
//                       borderSide: BorderSide(color: AppColors.primaryColor, width: 2.0),
//                     ),
//                     ),
//                   ),
//                   SizedBox(height: 10),
//                   TextField(
//                     controller: _villeController,
//                     style: TextStyle(color: AppColors.textColor),
//                     decoration: InputDecoration(
//                       hintText: 'Ville',
//                       contentPadding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
//                       border: OutlineInputBorder(
//                       borderRadius: BorderRadius.all(Radius.circular(4)),
//                     ),
//                     filled: true,
//                     fillColor:AppColors.FlutterDefautBackground,
//                     hintStyle: TextStyle(color: AppColors.textColor),
//                     disabledBorder: OutlineInputBorder(
//                       borderSide: BorderSide(color: AppColors.textColor),
//                     ),
//                     focusedBorder: OutlineInputBorder(
//                       borderSide: BorderSide(color: AppColors.primaryColor, width: 2.0),
//                     ),
//                     ),
//                   ),
//                   SizedBox(height: 10),
//                   TextField(
//                     controller: _paysController,
//                     style: TextStyle(color: AppColors.textColor),
//                     decoration: InputDecoration(
//                       hintText: 'Pays',
//                       contentPadding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
//                       border: OutlineInputBorder(
//                       borderRadius: BorderRadius.all(Radius.circular(4)),
//                     ),
//                     filled: true,
//                     fillColor:AppColors.FlutterDefautBackground,
//                     hintStyle: TextStyle(color: AppColors.textColor),
//                     disabledBorder: OutlineInputBorder(
//                       borderSide: BorderSide(color: AppColors.textColor),
//                     ),
//                     focusedBorder: OutlineInputBorder(
//                       borderSide: BorderSide(color: AppColors.primaryColor, width: 2.0),
//                     ),
//                     ),
//                   ),
//                 ],
//               ),
//             ),
//           ),
//         ),
//         Row(
//   mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//   children: [
//     SizedBox(
//       width: MediaQuery.of(context).size.width * 0.9,
//       child: ElevatedButton(
//         onPressed: () {

// void modifyClient(String uriPath, Map<String, dynamic> body) {
//   final id = (widget.selectedClient['thirdPartyId']).toInt();
//   final uri = Uri.parse('https://internaldemo.adisoft.app/ui/signaletique/thirdparty/update/$id/$uriPath');
  
//   http.put(
//     uri,
//     headers: {
//       'Content-Type': 'application/json',
//       'Referer': 'https://internaldemo.adisoft.app',
//       'Cookie': LoginResponse.cookie,
//     },
//     body: jsonEncode(body),
//   ).then((response) {
//     if (response.statusCode == 200) {
//       print("insert successful");

//     } else {
//       print('Error: ${response.statusCode}');
//     }
//   }).catchError((e) {
//     print('Exception error: $e');
//   });
// }

// // Define a map containing all the controllers
// final Map<String, TextEditingController> controllers = {
//   'primaryName': _nomController,
//   'vatNumber': _numeroTVAController,
//   'generalMail': _mailController,
//   'generalPhone': _phoneController,
//   'street': _rueController,
//   'streetNum': _numeroController,
//   'postalBox': _BPController,
//   'postalCode': _CPController,
//   'city': _villeController,
//   'country': _paysController,
// };

// List<String> uriP = [
//   'primaryname',
//   'vatnumber',
//   'generalmail',
//   'generalphone',
//   'street',
//   'streetnum',
//   'postalbox',
//   'postalcode',
//   'city',
//   'country',
// ];

// controllers.forEach((key, controller) {
//   modifyClient(uriP[controllers.keys.toList().indexOf(key)], {key: controller.text});
// });
// Navigator.of(context).pushReplacement(
//   MaterialPageRoute(builder: (context) => pagelisteDesClients()),
// );
//         },
//         style: ElevatedButton.styleFrom(
//           backgroundColor: AppColors.primaryColor, 
//           foregroundColor: AppColors.white, 
//           shape: RoundedRectangleBorder(
//             borderRadius: BorderRadius.circular(4), 
//           ),
//         ),
//         child: Row(
//           mainAxisAlignment: MainAxisAlignment.center,
//           children: [
//             Text('Confirmer   '),
//             Icon(Icons.check),
//           ],
//         ),
//       ),
//     ),
//   ],
// ),

//       ],
//     ),
    
//     bottomNavigationBar: BottomNavigationBar(
//       currentIndex: _selectedIndex,
//       onTap: _navigateBottomBar,
//       type: BottomNavigationBarType.fixed,
//       items: const [
//         BottomNavigationBarItem(icon: Icon(Icons.home), label: 'QR code'),
//         BottomNavigationBarItem(icon: Icon(Icons.message), label: 'Statistiques'),
//         BottomNavigationBarItem(icon: Icon(Icons.add), label: 'Accueil'),
//         BottomNavigationBarItem(icon: Icon(Icons.notifications), label: 'Fournisseurs'),
//         BottomNavigationBarItem(icon: Icon(Icons.person), label: 'Clients'),
//       ],
//       selectedItemColor: AppColors.primaryColor,
//       unselectedItemColor: AppColors.primaryColorDisable,
//     ),
//   );
// }

// }
