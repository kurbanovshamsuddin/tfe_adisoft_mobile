// // ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables

// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables, library_private_types_in_public_api

import 'dart:convert';

import 'package:adisoft_mobile/pages/Login/db_connection.dart';
import 'package:adisoft_mobile/pages/PagesClientsFournisseurs/FluxClients/PagesListe/listeDesClients.dart';
import 'package:flutter/material.dart';
import 'package:adisoft_mobile/FuntionsGlobal.dart';
import 'package:adisoft_mobile/globalColors.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;

class pageAfficheOffre extends StatefulWidget {
    
  final Map<String, dynamic> selectedOffer;
  const pageAfficheOffre({Key? key, required this.selectedOffer}) : super(key: key);




  @override
  _pageAfficheOffreState createState() => _pageAfficheOffreState();
}

class UpperCaseTextFormatter extends TextInputFormatter {
  @override
  TextEditingValue formatEditUpdate(TextEditingValue oldValue, TextEditingValue newValue) {
    return TextEditingValue(
      text: newValue.text.toUpperCase(),
      selection: newValue.selection,
    );
  }
}

  

class _pageAfficheOffreState extends State<pageAfficheOffre> {
int _selectedIndex = 0;
String numeroTVA = "";
int resultat = 0;
Color borderColor = AppColors.disabledTextColor;
List<String> options = List.empty();
List<String> refOptions = List.empty();

  @override
  void initState() {
    super.initState();
    getClientList();
    getArtList();
  }

Future getClientList() async {
  final uri = Uri.parse('https://internaldemo.adisoft.app/ui/signaletique/customer/find-customers-by-folder-tenant-id?folderTenantId=1');
  try {
    final response = await http.get(
      uri,
      headers: {
        'Content-Type': 'application/json',
        'Referer': 'https://internaldemo.adisoft.app',
        'Cookie': LoginResponse.cookie,
      },
    );
    if (response.statusCode == 200) {
        Map<String, dynamic> responseMap = json.decode(response.body);
        setState(() {
          options = responseMap['response'].map<String>((value) {
            // Extract specific values and concatenate them into one string
            return '${value['primaryName']}, ${value['shortName']}, ${value['adress']}';
          }).toList();

          for (int i = 0; i < options.length; i++) {
            if (options[i].contains("null")) {
              options[i] = options[i].replaceAll(", null", '');
            }
          }
        });

  options.forEach((option) {
    print(option);
  });
    } else {
      print('Error: ${response.statusCode}');
    }
    return response;
  } catch (e) {
    print('Exception error: $e');
    return null;
  }
}

Future getArtList() async {
  final uri = Uri.parse('https://internaldemo.adisoft.app/ui/businessmanagement/article/find-actives?filterMacroArticle=false&filterTechnicals=false');
  try {
    final response = await http.get(
      uri,
      headers: {
        'Content-Type': 'application/json',
        'Referer': 'https://internaldemo.adisoft.app',
        'Cookie': LoginResponse.cookie,
        "Adisoft_folder_tenant": '1',
      },
    );
    if (response.statusCode == 200) {
        Map<String, dynamic> responseMap = json.decode(response.body);
        setState(() {
          refOptions = responseMap['response'].map<String>((value) {
            // Extract specific values and concatenate them into one string
            return '${value['ref']}';
          }).toList();
        });

  refOptions.forEach((refOptions) {
    print(refOptions);
  });

    } else {
      print('Error: ${response.statusCode}');
    }
    return response;
  } catch (e) {
    print('Exception error: $e');
    return null;
  }
}



  var nomClient  = TextEditingController();
  var dateDocument  = TextEditingController();
  var dateEcheance  = TextEditingController();
  var encodageTVAC  = TextEditingController();
  var refArticle  = TextEditingController();
  var descriptif = TextEditingController();
  var quantite  = TextEditingController();
  var prixTVAC   = TextEditingController();
  var remise  = TextEditingController();
  var totalTVAC   = TextEditingController();
  var tauxTVA   = TextEditingController();
  var noteDeLaLigne   = TextEditingController();

  void _navigateBottomBar(int index) {
    setState(() {
      _selectedIndex = index;
      footerNavigationSwitch(_selectedIndex, context);
    });
  }

@override
Widget build(BuildContext context) {
  return Scaffold(
    appBar: AppBar(
      leading: BackButton(
        color: AppColors.white,
      ),
      title: const Text("", style: TextStyle(color: Color.fromARGB(255, 17, 16, 16))),
      centerTitle: true,
      backgroundColor: AppColors.primaryColor,
    ),
    body: Column(
      children: [
        // Fixed content at the top
        Padding(
          padding: const EdgeInsets.all(10),
          child: Center(
            child: RichText(
              text: TextSpan(
                children: [
                  TextSpan(
                    text: 'Nouveau ',
                    style: const TextStyle(
                      fontSize: 30,
                      color: AppColors.textColor,
                    ),
                  ),
                  TextSpan(
                    text: 'offre',
                    style: const TextStyle(
                      fontSize: 30,
                      color: AppColors.primaryColor,
                    ),
                  ),
                ],
              ),
              overflow: TextOverflow.ellipsis,
            ),
          ),
        ),
        // Scrollable content
         Expanded(
          child: SingleChildScrollView(
            child: Padding(
              padding: EdgeInsets.symmetric(vertical: 20,horizontal: 20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  // buildDropdownField(options, "Nom client", "Client"),
                  SizedBox(height: 10),
                TextField(
                  controller: dateDocument,
                  style: TextStyle(color: AppColors.disabledTextColor),
                  decoration: InputDecoration(
                    hintText: 'Date document',
                    contentPadding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(4)),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: AppColors.primaryColor, width: 2.0),
                    ),
                  ),
                ),
                SizedBox(height: 10),
                TextField(
                  controller: dateEcheance,
                  style: TextStyle(color: AppColors.disabledTextColor),
                  decoration: InputDecoration(
                    hintText: 'Date échéance',
                    contentPadding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(4)),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: AppColors.primaryColor, width: 2.0),
                    ),
                  ),
                ),

                  SizedBox(height: 10),
                  TextField(
                    controller: encodageTVAC,
                      style: TextStyle(color: AppColors.disabledTextColor),
                      decoration: InputDecoration(
                      hintText: 'Encodage TVAC',
                      contentPadding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
                      border: OutlineInputBorder( // Customize the border
                      borderRadius: BorderRadius.all(Radius.circular(4)),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: AppColors.primaryColor, width: 2.0), // Border color when focused
                    ),
                    ),
                  ),
                  SizedBox(height: 10),
                  Center( 
                  child: Text(
                    "Article 1",
                    style: TextStyle(
                      fontSize: 30,
                      color: AppColors.primaryColor,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                ),
                  SizedBox(height: 10),
                  // buildDropdownField(refOptions, "Nom article", "Article"),
                  SizedBox(height: 10),
                  TextField(
                    controller: descriptif,
                    style: TextStyle(color: AppColors.disabledTextColor),
                    decoration: InputDecoration(
                      hintText: 'Descriptif',
                      contentPadding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
                      border: OutlineInputBorder( // Customize the border
                      borderRadius: BorderRadius.all(Radius.circular(4)),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: AppColors.primaryColor, width: 2.0), // Border color when focused
                    ),
                    ),
                  ),
                  SizedBox(height: 10),
                  Row(
                    children: [
                      Expanded(
                        child: TextField(
                          controller: quantite ,
                          style: TextStyle(color: AppColors.disabledTextColor),
                          decoration: InputDecoration(
                            hintText: 'Quantité',
                            contentPadding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
                      border: OutlineInputBorder( // Customize the border
                      borderRadius: BorderRadius.all(Radius.circular(4)),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: AppColors.primaryColor, width: 2.0), // Border color when focused
                    ),
                          ),
                        ),
                      ),
                      SizedBox(width: 10),
                      Expanded(
                        child: TextField(
                          controller: prixTVAC,
                          style: TextStyle(color: AppColors.disabledTextColor),
                          decoration: InputDecoration(
                            hintText: 'Prix TVAC',
                            contentPadding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
                      border: OutlineInputBorder( // Customize the border
                      borderRadius: BorderRadius.all(Radius.circular(4)),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: AppColors.primaryColor, width: 2.0), // Border color when focused
                    ),
                          ),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 10),
                  Row(
                    children: [
                      Expanded(
                        child: TextField(
                          controller: remise ,
                          style: TextStyle(color: AppColors.disabledTextColor),
                          decoration: InputDecoration(
                            hintText: 'Remise',
                            contentPadding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
                      border: OutlineInputBorder( // Customize the border
                      borderRadius: BorderRadius.all(Radius.circular(4)),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: AppColors.primaryColor, width: 2.0), // Border color when focused
                    ),
                          ),
                        ),
                      ),
                      SizedBox(width: 10),
                      Expanded(
                        child: TextField(
                          controller: totalTVAC,
                          style: TextStyle(color: AppColors.disabledTextColor),
                          decoration: InputDecoration(
                            hintText: 'Total TVAC',
                            contentPadding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
                      border: OutlineInputBorder( // Customize the border
                      borderRadius: BorderRadius.all(Radius.circular(4)),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: AppColors.primaryColor, width: 2.0), // Border color when focused
                    ),
                          ),
                        ),
                      ),
                    ],
                  ),

                  SizedBox(height: 10),
                  TextField(
                    controller: tauxTVA,
                    style: TextStyle(color: AppColors.disabledTextColor),
                    decoration: InputDecoration(
                      hintText: 'Taux de TVA',
                      contentPadding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
                      border: OutlineInputBorder( // Customize the border
                      borderRadius: BorderRadius.all(Radius.circular(4)),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: AppColors.primaryColor, width: 2.0), // Border color when focused
                    ),
                    ),
                  ),
                  SizedBox(height: 10),
                  TextField(
                    controller: noteDeLaLigne,
                    style: TextStyle(color: AppColors.disabledTextColor),
                    decoration: InputDecoration(
                      hintText: 'Note de la ligne',
                      contentPadding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
                      border: OutlineInputBorder( // Customize the border
                      borderRadius: BorderRadius.all(Radius.circular(4)),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: AppColors.primaryColor, width: 2.0), // Border color when focused
                    ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
Container(
  padding: EdgeInsets.symmetric(vertical: 10,horizontal: 10), // 10 padding on each side
  child: Row(
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    crossAxisAlignment: CrossAxisAlignment.center,
    children: [
      Expanded(
        child: ElevatedButton(
          onPressed: () {
          Future addOffer() async {
            final uri = Uri.parse('https://internaldemo.adisoft.app/ui/signaletique/thirdparty/create-third-party-customer?folderTenantId=1');
            try {
              final response = await http.post(
                uri,
                headers: {
                  'Content-Type': 'application/json',
                  'Referer': 'https://internaldemo.adisoft.app',
                  'Cookie': LoginResponse.cookie,
                },
                body: jsonEncode({
                  {
                    "document": {
                      "id": 1253,
                      "lastUpdateDateTime": "2024-04-30T07:34:07.208772Z", //
                      "businessJournalId": 2, //??
                      "documentNumberingFormat": "YY--Nu", //
                      "inclVatEncoding": "TVAC", //
                      "totalVatAmount": 30.08, //
                      "totalAmountExclVat": 501.35, //
                      "totalAmountInclVat": 531.43, //
                      "folderTenantId": 1,
                      "documentRef": "2024-7", //
                      "documentCreationDate": "2024-04-30", // date doc
                      "documentDueDate": "2024-05-10", // date eche
                      "userIdCreation": 71, //
                      "userIdLastModification": 71, //
                      "encodingPeriodId": 61, // gener tout seul
                      "thirdPartyId": 1, //
                      "sendingMailUUID": "1443a95a-080f-40cf-bb08-40c97ee94ab8",//?
                      "businessFolderJournal": {
                        "id": 2,
                        "creationDateTime": "2022-08-26T07:19:11.628494Z",
                        "lastUpdateDateTime": "2023-12-29T22:12:35.454803Z",
                        "mnemonic": "OFFRE",
                        "frenchName": "Offre",
                        "dutchName": "Offerte",
                        "englishName": "Offer",
                        "folderTenantId": 1,
                        "journalType": "OFFER",
                        "documentNumberingFormat": "YY--Nu",
                        "stockInfluence": false,
                        "vcsActivated": false,
                        "folderJournalId": 2,
                        "defaultWarehouseEditable": false,
                        "officialName": "OFFRE - Offre",
                        "name": "Offre"
                      }
                    },
                    "documentLines": [
                      {
                        "unitPriceExclVat": 25.384905,
                        "totalAmountExclVat": 501.351874,
                        "discountPercentage": 21,
                        "linkedToTechnicalArticle": false, // toujours false interne
                        "unitPriceInclVat": 26.907999,
                        "unitVatAmount": 1.523094,
                        "totalVatAmount": 30.081112,
                        "discountAmountExclVat": 5.33083,
                        "discountTotalAmountExclVat": 133.270751,
                        "label": "farine poulet bio Descriptif",
                        "quantity": 25,
                        "vatRate": "S600",
                        "articleId": 389,
                        "totalAmountInclVat": 531.432986,
                        "labelIncluded": true,
                        "articleRef": "actipoulet",
                        "article": {
                          "id": 389,
                          "creationDateTime": "2023-11-27T13:26:24.315719Z",
                          "ref": "actipoulet",
                          "mainLabel": "farine poulet bio",
                          "textDescriptionUsedInDocument": "ONLY_LABEL",
                          "unit": "KILOGRAM",
                          "vatRate": "S600",
                          "sellingPriceExclVAT": 25.384905,
                          "purchasingPriceExclVAT": 19.22,
                          "salePackagingQuantity": 0,
                          "saleMinQuantity": 0,
                          "defaultQuantity": 25,
                          "technicalArticle": false,
                          "quantityNumberDecimals": 2,
                          "generalAccountIdSale": 223,
                          "generalAccountIdPurchase": 131,
                          "folderTenantId": 1,
                          "stockEnabled": false,
                          "name": "actipoulet - farine poulet bio"
                        },
                        "articleQuantityNumberDecimal": 2,
                        "effectiveVatRate": "S600",
                        "articleMeasureUnit": "KILOGRAM",
                        "internalNote": "note à la ligne test"
                      }
                    ],
                    "deletedLines": []
                  },

                  // 'primaryName': nomClient.text,
                  // 'vatNumber': dateDocument.text,
                  // 'generalMail': dateEcheance.text,//
                  // 'generalPhone': encodageTVAC.text,//
                  // 'street': refArticle.text,
                  // 'streetNum': descriptif.text,//
                  // 'postalBox': quantite.text,//
                  // 'postalCode': prixTVAC.text,//
                  // 'city': remise.text,
                  // 'country': totalTVAC.text,
                  // 'country': tauxTVA.text,
                  // 'country': noteDeLaLigne.text,
                }),
              );
              if (response.statusCode == 200) {
                print("insert succesull");
                Navigator.of(context).pushReplacement(
                  MaterialPageRoute(builder: (context) => pagelisteDesClients()),
                );
                // Map<String, dynamic> jsonResponse = json.decode(response.body);
                // userId = jsonResponse['response']['userId'];
              } else {
                print('Error: ${response.statusCode}');
              }
              
              return null;
              
            } catch (e) {
              print('Exception error: $e');
              return null;
            }
          }

          addOffer();

          },
          style: ElevatedButton.styleFrom(
            backgroundColor: AppColors.primaryColor, 
            foregroundColor: AppColors.white, 
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(4), 
            ),
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text('Confirmer  '),
              Icon(Icons.check),
            ],
          ),
        ),
      ),
    ],
  ),
),

      ],
    ),
    bottomNavigationBar: BottomNavigationBar(
      currentIndex: _selectedIndex,
      onTap: _navigateBottomBar,
      type: BottomNavigationBarType.fixed,
      items: const [
        BottomNavigationBarItem(icon: Icon(Icons.home), label: 'QR code'),
        BottomNavigationBarItem(icon: Icon(Icons.message), label: 'Statistiques'),
        BottomNavigationBarItem(icon: Icon(Icons.add), label: 'Accueil'),
        BottomNavigationBarItem(icon: Icon(Icons.notifications), label: 'Fournisseurs'),
        BottomNavigationBarItem(icon: Icon(Icons.person), label: 'Clients'),
      ],
      selectedItemColor: AppColors.primaryColor,
      unselectedItemColor: AppColors.primaryColorDisable,
    ),
  );
}

}
