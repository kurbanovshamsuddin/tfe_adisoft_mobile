// ignore_for_file: prefer_const_literals_to_create_immutables, prefer_const_constructors, camel_case_types, prefer_final_fields, library_private_types_in_public_api, use_super_parameters, avoid_print, file_names

import 'dart:convert';
import 'package:adisoft_mobile/pages/Login/db_connection.dart';
import 'package:adisoft_mobile/pages/PagesClientsFournisseurs/FluxClients/CRUDoffre/afficheOffre.dart';

import 'package:adisoft_mobile/pages/PagesClientsFournisseurs/FluxClients/CRUDoffre/nouveauOffre.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import 'package:adisoft_mobile/FuntionsGlobal.dart';
import 'package:adisoft_mobile/globalColors.dart';
import 'package:intl/intl.dart';

class pagelisteOffre extends StatefulWidget {
  const pagelisteOffre({Key? key}) : super(key: key);

  @override
  _pagelisteOffreState createState() => _pagelisteOffreState();
}
String getCurrectData() {
  DateTime now = DateTime.now();
  String formattedDate = DateFormat("yyyy-MM-ddTHH:mm:ss.SSS").format(now);
  print(formattedDate);
  return formattedDate;
}
class _pagelisteOffreState extends State<pagelisteOffre> {
  int _selectedIndex = 4;
  late String idparty = '';
  late Map<String, dynamic> responseMap = {};
  List<Map<String, dynamic>> _tableData = [];
  TextEditingController _searchController = TextEditingController();



  
  @override
  void initState() {
    super.initState();
    
    fetchData();

  }

  Future<void> fetchData() async {
    final uri = Uri.parse('https://internaldemo.adisoft.app/ui/businessmanagement/businessdocument/initiate-business-documents');
    final response = await http.post(
      uri,
      headers: {
        'Content-Type': 'application/json',
        'Referer': 'https://internaldemo.adisoft.app',
        "Adisoft_folder_tenant": "1",
        'Cookie': LoginResponse.cookie,
      },
      body: jsonEncode({
      "archivedValue": false,
      "businessDocumentFilterType": 0,
      "businessFolderJournalIds": [2],
      "currentToggleValue": false,
      "from": "2000-01-01T01:00:00.000",
      "to": getCurrectData(),
      "mailStatusTypesFilters": [],
      "statusFilters": [],
    }),
    );
    if (response.statusCode == 200) {
      // Successful response
      List<Map<String, dynamic>> dataList = [];
      final Map<String, dynamic> responseData = json.decode(utf8.decode(response.bodyBytes));
      final List<dynamic> responseList = responseData['response'];
      for (var item in responseList) {
        if (item is Map<String, dynamic>) {
          if (item['deactivationDateTime'] == null) {
            dataList.add(item);
          }
        }
      }
      setState(() {
        _tableData = dataList;
        var clientsThirdPartyId = <int, int>{};
          for (var thirdPartyId in _tableData) {

            int currentThirdPartyId = thirdPartyId['thirdPartyId'].toInt();
            clientsThirdPartyId[clientsThirdPartyId.length] = currentThirdPartyId;
          }
          // print(clientsThirdPartyId);
      });
    } else {
      // Handle error
      print('Error: ${response.statusCode}');
    }
  }

  List<Map<String, dynamic>> _filterData(String query) {
    return _tableData.where((item) {
      // Combine all fields into a single string for searching
      String combinedInfo =
          "${item['documentRef']} ${item['thirdPartyName']} ${item['documentCreationDate']} ${item['totalAmountExclVat']}"
              .toLowerCase();
      return combinedInfo.contains(query.toLowerCase());
    }).toList();
  }

  void _navigateBottomBar(int index) {
    setState(() {
      _selectedIndex = index;
      footerNavigationSwitch(_selectedIndex, context);
    });
  }
  void _handleRowClick(int index) {
    print(index);
    final selectedOffer = _filterData(_searchController.text)[index]; // Retrieve the entire item
    Navigator.of(context).push(

      MaterialPageRoute(builder: (context) => pageAfficheOffre(selectedOffer: selectedOffer)),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: BackButton(
          color: AppColors.white,
        ),
        title: const Text("Flux clients",
            style: TextStyle(color: AppColors.white)),
        centerTitle: true,
        backgroundColor: AppColors.primaryColor,
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          SizedBox(height: 5),
          RichText(
            text: TextSpan(
              children: [
                TextSpan(
                  text: 'Liste des ',
                  style: TextStyle(
                    fontSize: 36,
                    color: AppColors.textColor,
                    height: 2,
                  ),
                ),
                TextSpan(
                  text: 'offres',
                  style: TextStyle(
                    fontSize: 36,
                    color: AppColors.primaryColor, 
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height: 5),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10),
            child: Row(
              children: [
                Expanded(
                  child: TextField(
                    controller: _searchController,
                    onChanged: (value) {
                      setState(() {});
                    },
                    decoration: InputDecoration(
                      contentPadding: EdgeInsets.symmetric(vertical: 10),
                      hintText: 'Rechercher',
                      prefixIcon: Icon(Icons.search),
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(12),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(12),
                        borderSide: BorderSide(
                            width: 2,
                            color: AppColors.primaryColor),
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(12),
                        borderSide: BorderSide(width: 2, color: Colors.grey),
                      ),
                    ),
                  ),
                ),
                SizedBox(width: 5),
                ElevatedButton.icon(
                  onPressed: () {
                    Navigator.of(context).push(
                      MaterialPageRoute(builder: (context) => pageNouveauOffre()),
                    );
                  },
                  icon: Icon(
                    Icons.add,
                    size: 32,
                    color: Colors.white,
                  ),
                  label: Text(''),
                  style: ElevatedButton.styleFrom(
                    padding: EdgeInsets.symmetric(horizontal: 8, vertical: 8),
                    alignment: Alignment(1, 0),
                    backgroundColor: AppColors
                        .primaryColor, // Change this to your desired color
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(12),
                    ),
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height: 20),
          Expanded(
            child: ListView.builder(
              padding: EdgeInsets.all(15),
              itemCount: _filterData(_searchController.text).length,
              itemBuilder: (BuildContext context, int index) {
                final filteredData = _filterData(_searchController.text);
                var thirdPartyId = filteredData[index]['thirdPartyId'];
                for (var entry in responseMap.entries) {
                  double doubleValue = entry.value;
                  int keyValue = int.parse(entry.key);
                  int intThirdPartyId = thirdPartyId.toInt();
  
                  if (keyValue == intThirdPartyId){
                    int valueClient = doubleValue.toInt();
                    idparty = valueClient.toString();
                    break;
                  }else{
                    idparty = "";
                  }
                }
                                
                return GestureDetector(
                  onTap: () {
                    _handleRowClick(index);
                  },
                  child: Container(
                    decoration: BoxDecoration(
                      border: Border(
                        top: BorderSide(color: Colors.grey.withOpacity(0.5)),
                      ),
                    ),
                    child: Stack(
                      children: [
                        Row(
                          children: [
                            Expanded(
                              child: Card(
                                elevation: 0,
                                child: ListTile(
                                  contentPadding: EdgeInsets.zero,
                                  subtitle: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Row(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Expanded(
                                            child: Text(
                                              '${filteredData[index]['documentRef']}',
                                              overflow: TextOverflow.ellipsis,
                                              maxLines: 1,
                                            ),
                                          ),
                                          Expanded(
                                            child: Text(
                                              '${filteredData[index]['thirdPartyName']}',
                                              overflow: TextOverflow.ellipsis,
                                              maxLines: 1,
                                            ),
                                          ),
                                        ],
                                      ),
                                      Row(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Expanded(
                                            child: Text(
                                              '${filteredData[index]['documentCreationDate']}',
                                              overflow: TextOverflow.ellipsis,
                                              maxLines: 1,
                                            ),
                                          ),
                                          Expanded(
                                            child: Text(
                                              'HTVA: ${filteredData[index]['totalAmountExclVat']} €',
                                              overflow: TextOverflow.ellipsis,
                                              maxLines: 1,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                );
              },
            ),
          ),
        ],
      ),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _selectedIndex,
        onTap: _navigateBottomBar,
        type: BottomNavigationBarType.fixed,
        items: [
          BottomNavigationBarItem(icon: Icon(Icons.home), label: 'QR code'),
          BottomNavigationBarItem(icon: Icon(Icons.message), label: 'Statistiques'),
          BottomNavigationBarItem(icon: Icon(Icons.add), label: 'Accueil'),
          BottomNavigationBarItem(icon: Icon(Icons.notifications), label: 'Fournisseurs'),
          BottomNavigationBarItem(icon: Icon(Icons.person), label: 'Clients'),
        ],
        selectedItemColor: AppColors.primaryColor,
        unselectedItemColor: AppColors.primaryColorDisable,
      ),
    );
  }
}
