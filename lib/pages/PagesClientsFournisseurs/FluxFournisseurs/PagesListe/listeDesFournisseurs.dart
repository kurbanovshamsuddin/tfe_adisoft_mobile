// ignore_for_file: prefer_const_literals_to_create_immutables, prefer_const_constructors, camel_case_types, prefer_final_fields, library_private_types_in_public_api, use_super_parameters, avoid_print, file_names

import 'dart:convert';
import 'package:adisoft_mobile/pages/Login/db_connection.dart';
import 'package:adisoft_mobile/pages/PagesContact/listeDesContact.dart';
import 'package:adisoft_mobile/pages/PagesClientsFournisseurs/FluxFournisseurs/CRUDfournisseurs/afficheFournisseurs.dart';
import 'package:adisoft_mobile/pages/PagesClientsFournisseurs/FluxFournisseurs/CRUDfournisseurs/nouveauFournisseurs.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import 'package:adisoft_mobile/FuntionsGlobal.dart';
import 'package:adisoft_mobile/globalColors.dart';

class pagelisteDesFournisseurs extends StatefulWidget {
  const pagelisteDesFournisseurs({Key? key}) : super(key: key);

  @override
  _pagelisteDesFournisseursState createState() => _pagelisteDesFournisseursState();
}

class _pagelisteDesFournisseursState extends State<pagelisteDesFournisseurs> {
  int _selectedIndex = 3;
  late String Idparty = '';
  late Map<String, dynamic> responseMap = {};
  List<Map<String, dynamic>> _tableData = [];
  TextEditingController _searchController = TextEditingController();
  @override
  void initState() {
    super.initState();
    fetchData();
  }

  Future<Null> countContact(Map<int, int> clientsThirdPartyId) async {
  List<int> serializableMap = clientsThirdPartyId.values.toList();
  final uri = Uri.parse('https://internaldemo.adisoft.app/ui/signaletique/thirdparty/get-number-contacts-of-third-parties');
  try {
    final response = await http.post(
      uri,
      headers: {
        'Content-Type': 'application/json',
        'Referer': 'https://internaldemo.adisoft.app',
        'Cookie': LoginResponse.cookie,
      },
      body: jsonEncode(serializableMap),
    );
    if (response.statusCode == 200) {
      print("nombre de contact bien reçu");
      Map<String, dynamic> responseBody = jsonDecode(response.body);
 
      // responseMap = responseBody['response'];
    setState(() {
      responseMap = responseBody['response'];  // Assuming responseMap holds the count
    });
      print(responseMap);
    } else {
      print('Error: ${response.statusCode}');
    }
    
    return null;
    
    } catch (e) {
      print('Exception error: $e');
      return null;
    }
  }

  Future<void> fetchData() async {
    final uri = Uri.parse(
        'https://internaldemo.adisoft.app/ui/signaletique/provider/find-providers-by-folder-tenant-id?folderTenantId=1');
    final response = await http.get(
      uri,
      headers: {
        'Content-Type': 'application/json',
        'Referer': 'https://internaldemo.adisoft.app',
        'Cookie': LoginResponse.cookie,
      },
    );

    if (response.statusCode == 200) {
      // Successful response
            List<Map<String, dynamic>> dataList = [];
      final Map<String, dynamic> responseData = json.decode(utf8.decode(response.bodyBytes));
           final List<dynamic> responseList = responseData['response'];
      for (var item in responseList) {
        if (item is Map<String, dynamic>) {
          if (item['deactivationDateTime'] == null) {
            dataList.add(item);
          }
        }
      }
      setState(() {
        _tableData = dataList;
        var clientsThirdPartyId = <int, int>{};
          for (var thirdPartyId in _tableData) {

            int currentThirdPartyId = thirdPartyId['thirdPartyId'].toInt();
            clientsThirdPartyId[clientsThirdPartyId.length] = currentThirdPartyId;
          }
          countContact(clientsThirdPartyId);
          // print(clientsThirdPartyId);
      });
     
     
     
     
     
      // final List<dynamic> responseList = responseData['response'];
      // final List<Map<String, dynamic>> dataList =
      //     responseList.map((item) => item as Map<String, dynamic>).toList();
      // setState(() {
      //   _tableData = dataList;
      // });
    } else {
      // Handle error
      print('Error: ${response.statusCode}');
    }
  }

  List<Map<String, dynamic>> _filterData(String query) {
    return _tableData.where((item) {
      // Combine all fields into a single string for searching
      String combinedInfo =
          "${item['primaryName']} ${item['vatNumber']} ${item['adress']}"
              .toLowerCase();
      return combinedInfo.contains(query.toLowerCase());
    }).toList();
  }

  void _navigateBottomBar(int index) {
    setState(() {
      _selectedIndex = index;
      footerNavigationSwitch(_selectedIndex, context);
    });
  }

  void _handleIconClick(int index) {
    print('Icon at index $index is clicked.');


    final selectedClient = _filterData(_searchController.text)[index];  
    Navigator.of(context).push(
      MaterialPageRoute(builder: (context) => pagelisteDesContacts(selectedContact: selectedClient))
    );
  }

  void _handleRowClick(int index) {
    final selectedClient = _filterData(_searchController.text)[index]; // Retrieve the entire item
    Navigator.of(context).push(
      MaterialPageRoute(builder: (context) => pageafficheFournisseurs(selectedClient: selectedClient))
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: BackButton(
          color: AppColors.white, //change your color here
        ),
        title: const Text("Liste des tiers",
            style: TextStyle(color: AppColors.white)),
        centerTitle: true,
        backgroundColor: AppColors.primaryColor,
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          SizedBox(height: 5),
          RichText(
            text: TextSpan(
              children: [
                TextSpan(
                  text: 'Liste des ',
                  style: TextStyle(
                    fontSize: 32,
                    color: AppColors.textColor,
                    height: 2,
                  ),
                ),
                TextSpan(
                  text: 'Fournisseurs',
                  style: TextStyle(
                    fontSize: 32,
                    color: AppColors.primaryColor, // Color for "articles"
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height: 5),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10),
            child: Row(
              children: [
                Expanded(
                  child: TextField(
                    controller: _searchController,
                    onChanged: (value) {
                      setState(() {});
                    },
                    decoration: InputDecoration(
                      contentPadding: EdgeInsets.symmetric(vertical: 10),
                      hintText: 'Rechercher',
                      prefixIcon: Icon(Icons.search),
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(12),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(12),
                        borderSide: BorderSide(
                            width: 2,
                            color: AppColors
                                .primaryColor), // Change this to your desired color
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(12),
                        borderSide: BorderSide(width: 2, color: Colors.grey),
                      ),
                    ),
                  ),
                ),
                SizedBox(width: 5),
                ElevatedButton.icon(
                  onPressed: () {
                    Navigator.of(context).push(
                      MaterialPageRoute(builder: (context) => pageNouveaufournisseur()),
                    );
                  },
                  icon: Icon(
                    Icons.add,
                    size: 32,
                    color: Colors.white,
                  ),
                  label: Text(''),
                  style: ElevatedButton.styleFrom(
                    padding: EdgeInsets.symmetric(horizontal: 8, vertical: 8),
                    alignment: Alignment(1, 0),
                    backgroundColor: AppColors
                        .primaryColor, // Change this to your desired color
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(12),
                    ),
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height: 20),
          Expanded(
            child: ListView.builder(
              padding: EdgeInsets.all(15),
              itemCount: _filterData(_searchController.text).length,
              itemBuilder: (BuildContext context, int index) {
                final filteredData = _filterData(_searchController.text);
                var thirdPartyId = filteredData[index]['thirdPartyId'];
                for (var entry in responseMap.entries) {
                  double doubleValue = entry.value;
                  int keyValue = int.parse(entry.key);
                  int intThirdPartyId = thirdPartyId.toInt();
  
                  if (keyValue == intThirdPartyId){
                    int valueClient = doubleValue.toInt();
                    Idparty = valueClient.toString();
                    break;
                  }else{
                    Idparty = "";
                  }
                }
                return GestureDetector(
                  onTap: () {
                    _handleRowClick(index);
                  },
                  child: Container(
                    decoration: BoxDecoration(
                      border: Border(
                        top: BorderSide(color: Colors.grey.withOpacity(0.5)),
                      ),
                    ),
                    child: Stack(
                      // Wrap your list item in a Stack widget
                      children: [
                        Row(
                          children: [
                            Expanded(
                              child: Card(
                                elevation: 0,
                                child: ListTile(
                                  contentPadding: EdgeInsets.zero,
                                  subtitle: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Row(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Expanded(
                                            child: Text(
                                              '${filteredData[index]['primaryName']}',
                                              overflow: TextOverflow.ellipsis,
                                              maxLines: 1,
                                            ),
                                          ),
                                          Expanded(
                                            child: Text(
                                              '${filteredData[index]['vatNumber']}',
                                              overflow: TextOverflow.ellipsis,
                                              maxLines: 1,
                                            ),
                                          ),
                                        ],
                                      ),
                                      Text(
                                        '${filteredData[index]['adress']}',
                                        overflow: TextOverflow.ellipsis,
                                        maxLines: 1,
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            IconButton(
                              onPressed: () {
                                _handleIconClick(index);
                              },
                              icon: Stack(
                                alignment: Alignment.topRight,
                                children: [
                                  Image.asset(
                                    'assets/images/contact.png',
                                    width: 48.0,
                                    height: 48.0,
                                  ),
                                  Visibility(
                                    visible: Idparty.isNotEmpty, // Show only if Idparty is not empty
                                    child: Container(
                                      padding: EdgeInsets.all(6),
                                      decoration: BoxDecoration(
                                        shape: BoxShape.circle,
                                        color: AppColors.primaryColor,
                                      ),
                                      child: Text(
                                        Idparty,
                                        style: TextStyle(
                                          fontSize: 14,
                                          color: AppColors.white,
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                );
              },
            ),
          ),
        ],
      ),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _selectedIndex,
        onTap: _navigateBottomBar,
        type: BottomNavigationBarType.fixed,
        items: [
          BottomNavigationBarItem(icon: Icon(Icons.home), label: 'QR code'),
          BottomNavigationBarItem(icon: Icon(Icons.message), label: 'Statistiques'),
          BottomNavigationBarItem(icon: Icon(Icons.add), label: 'Accueil'),
          BottomNavigationBarItem(icon: Icon(Icons.notifications), label: 'Fournisseurs'),
          BottomNavigationBarItem(icon: Icon(Icons.person), label: 'Clients'),
        ],
        selectedItemColor: AppColors.primaryColor,
        unselectedItemColor: AppColors.primaryColorDisable,
      ),
    );
  }
}
