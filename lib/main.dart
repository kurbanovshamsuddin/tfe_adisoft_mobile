// -*- coding: utf-8 -*-

import 'package:flutter/material.dart';
import 'pages/Login/login_page.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        fontFamily: 'Manrope',
        textTheme: const TextTheme(
          bodyMedium: TextStyle(fontFamily: 'Manrope'),
        ),
      ),
      home: LoginPage(),
    );
  }
}

